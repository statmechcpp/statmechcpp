#include <cstdlib>
#include <sstream>              // for std::ostringstream
#include <string>
#include <iostream>
#include <cassert>
#include <boost/program_options.hpp>
#include "../kernels/CKernelNN_2D_4_PBC.hpp"
#include "../kernels/CKernelBorile4.hpp"
#include "../gsl/CGSLRng.hpp"
#include "../gsl/CLogHisto.hpp"
#include "../absorbent/CAbsorbentLattice2D.hpp"
#include "CVoterModel2D.hpp"

#ifdef PLOT
#define DO_PLOT true 
#else
#define DO_PLOT false
#endif

#ifdef INTERNAL_MEASURES
#define DO_INTERNAL_MEASURES true
#else
#define DO_INTERNAL_MEASURES false
#endif

int
main (int ac, char *av[])
{
  typedef bool value_type; // two different values are enough
  typedef CAbsorbentLattice2D<value_type, DO_PLOT> lattice_type;

#ifdef B_VOTER
  typedef CKernelBorile4<lattice_type> kernel_type;
#else
  typedef CKernelNN_2D_4_PBC<lattice_type::index> kernel_type;
#endif
 
  typedef CVoterModel2D<lattice_type, kernel_type> voter_type;

  // ------- Parameters ----------
  size_t side1 = 20, side2 = 20;
  double epsilon = -1.;
  size_t n_iterations = 1;

  // ----- Parse CMD options ---------
  namespace po = boost::program_options;
  po::options_description desc("Allowed options");
  desc.add_options()
    ("help", "produce help message")
    ("x-side,x", po::value<size_t>(&side1), "set lattice width")
    ("y-side,y", po::value<size_t>(&side2), "set lattice height (width by default)")
#ifdef B_VOTER
    ("epsilon,e", po::value<double>(&epsilon), "set parameter")
#endif
    ("iterations,n", po::value<size_t>(&n_iterations),
     "set number of iterations (1 by default)")
    ;
  po::variables_map vm;
  po::store(po::parse_command_line(ac, av, desc), vm);
  po::notify(vm);  

  if (vm.count("help"))
    {
      std::cout << desc << "\n";
      return 1;
    }
  if (! vm.count("x-side"))
    {
      std::cout << "Error: lattice side must be specified.\n";
      std::cout << desc << "\n";
      return 1;
    }
  if (! vm.count("y-side"))
    {
      // same extension for y-side when it is not specified
      side2 = side1;
    }
#ifdef B_VOTER
  if (! vm.count("epsilon"))
    {
      std::cout << "Error: epsilon parameter must be specified\n";
      std::cout << desc << "\n";
      return 1;
    }
#endif

  // -------- Set main elements ------------
  lattice_type lattice(side1, side2, 2);   // the lattice
#ifdef B_VOTER
  const kernel_type k(lattice, epsilon);
#else
  const kernel_type k(side1, side2); // the kernel
#endif
  voter_type V(lattice, k, DO_INTERNAL_MEASURES);  // the dynamics

  voter_type::h_type & interphases = V.get_interphases ();
  //const voter_type::h_type & magnetization = V.get_magnetization ();
  std::ostringstream i_filename, m_filename, t_filename, p_filename;
  i_filename << "interphases";
  m_filename << "magnetization";
  t_filename << "time";
  p_filename << "survival_prob";

#ifndef B_VOTER // VOTER MODEL
  i_filename << "PBC4_" << side1 << "_" << side2;
  m_filename << "PBC4_" << side1 << "_" << side2;
  t_filename << "PBC4_" << side1 << "_" << side2;
  p_filename << "PBC4_" << side1 << "_" << side2;
#else // B_VOTER
    {
      const double alpha = ((double) side1) / ((double) side2);
      const size_t area = side1*side2;
      i_filename << "B4_" << alpha << "_" << epsilon << "_" << area;
      m_filename << "B4_" << alpha << "_" << epsilon << "_" << area;
      t_filename << "B4_" << alpha << "_" << epsilon << "_" << area;
      p_filename << "B4_" << alpha << "_" << epsilon << "_" << area;
    }
#endif
  {
    CGSLRng rng;
    const unsigned long int random_value = rng.uniform_int (10000000);
    i_filename << "_" << random_value << ".txt";
    m_filename << "_" << random_value << ".txt";
    t_filename << "_" << random_value << ".txt";
    p_filename << "_" << random_value << ".txt";
  }

  // ------ Loop --------

  // open a file for saving all the times
  std::ofstream t_file;
  t_file.open ( (t_filename.str()).c_str() );

  for (size_t current_it = 1; current_it <= n_iterations; ++current_it)
    {
      lattice.uniform_fill (); // give an initial state to the lattice
      const double t = V.StepsUntilDominance (); // measure time
      t_file << t << std::endl;

#ifdef INTERNAL_MEASURES
      // ------  print histograms --------

      // print density of interphases (instead of number of them)
      interphases.view_as_calls_smoother ();
      const size_t max_n_interphases = 2*side1*side2 - side1 - side2;
      interphases.set_accum_factor (1./(double) max_n_interphases);
      interphases.print ((i_filename.str()).c_str(), false, true);

      // print survival probability (using interphases histo !)
      interphases.view_as_calling_prob ();
      // I know the maths, but norma could be also be calculated by integration
      const double norm = (double) (side1*side2)/(double) current_it; 
      interphases.set_accum_factor (norm);
      interphases.print ((p_filename.str()).c_str(), false, true);

      // print mean magnetization
      //magnetization.set_accum_factor (1./(double) (area));
      //magnetization.print ((m_filename.str()).c_str(), false, true);
#endif
    };

  return 0;
}

