#ifndef CVOTERMODEL2D_HPP
#define CVOTERMODEL2D_HPP

#include "../gsl/CLogHisto.hpp"
#include "../observables/interphases.hpp"

template <typename LATTICE_TYPE, typename KERNEL_TYPE>
class CVoterModel2D
{
public:
  typedef CLogHisto<true, false> h_type;

private:
  typedef typename LATTICE_TYPE::node_type node_type;
  typedef typename LATTICE_TYPE::value_type value_type;

  LATTICE_TYPE & lattice; // take the working lattice as reference
  const KERNEL_TYPE & kernel;
  const bool is_m_interph;

  h_type interphases;
  h_type magnetization;

public:

  CVoterModel2D (LATTICE_TYPE & lattice, const KERNEL_TYPE &,
		 bool measure_interphases = false, double max_time = 1e15);
  // the factor that must be applied to StepsUntilDominance
  // for getting a measure of time independent of the size of the system
  double TimeFactor () const {return 1./(double) lattice.area ();}
  // run a VM and return the time needed to reach an absorbing state
  double StepsUntilDominance ();
  // run only one step
  void iterate_once ();

  h_type & get_interphases () {return interphases;}
  const h_type & get_magnetization () const {return magnetization;}
  const LATTICE_TYPE & get_lattice () const {return lattice;}
};

// WARNING: It works with PBC kernels, but you should make an specialization for other kernels (!)
template <typename LATTICE_TYPE, typename KERNEL_TYPE>
CVoterModel2D<LATTICE_TYPE,KERNEL_TYPE>::CVoterModel2D (LATTICE_TYPE & l,
						   const KERNEL_TYPE & k,
						   bool measure_interphases,
						   double max_time)
  : lattice(l),
    kernel (k),
    is_m_interph (measure_interphases),
    // will not measure before a # of steps shorter than total update.
    interphases (false, l.area (), max_time, l.area ()),
    magnetization (false, l.area (), max_time, l.area ())
{
  // ----- Set interphases histogram ------
  interphases.view_as_calls_smoother ();
  // consider input steps as divided by area: independent measure of time
  interphases.set_scale_factor (TimeFactor ());

  // ----- Set magnetization histogram -----
  magnetization.view_as_calls_smoother ();
  magnetization.set_scale_factor (TimeFactor ());
}

template <typename LATTICE_TYPE, typename KERNEL_TYPE>
void
CVoterModel2D<LATTICE_TYPE,KERNEL_TYPE>::iterate_once ()
{
  // avoid the use of 'new' implicitely when creating nodes,
  // that's very expensive here (profiled)
  node_type node;

  // get random node from the lattice
  lattice.random_node (&node);

#ifdef B_VOTER
  const value_type new_value4node = kernel.random_value (node);
  lattice.update_value (node, new_value4node); 
#else
  const value_type value4other_node = lattice.get_value (node);
  // choose a random neighbor of the first
  kernel.random_node (&node);
  // dynamics: assign value of first node to this neighbor
  lattice.update_value (node, value4other_node); 
#endif
}


template <typename LATTICE_TYPE, typename KERNEL_TYPE>
double
CVoterModel2D<LATTICE_TYPE,KERNEL_TYPE>::StepsUntilDominance ()
{
  size_t step = 1.;
  while (! lattice.monodominance ())
    {
      // measure interphases if
      //  1. requested, and
      //  2. after "every" node be updated "once"
      if (is_m_interph && 0 == (step % lattice.area ()))
	{
	  // Measure interphases
	  const size_t n_interphases = observables::m_interphases2D (lattice);
	  interphases.accumulate (step, n_interphases);
	  // Measure magnetization
	  const size_t nice_index = 0; // options are 0 or 1
	  magnetization.accumulate (step, lattice.get_abundance (nice_index));
	}
      // run dynamics
      iterate_once ();

      step++;
      assert (step != 0); // overflow!
    } // end while
  return step;
}

#endif
