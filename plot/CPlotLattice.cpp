#include "CPlotLattice.hpp"

// do_plot == false: do nothing
// do_plot == true: wait for a plot configuration
CPlotLattice::CPlotLattice (bool do_plot)
  : gp (do_plot),
    n_colors (0),
    xmin(0), xmax(0), ymin(0), ymax(0), 
    auto_refresh (true)
{
  // Init only if asked
  if (do_plot) reset ();
}

void
CPlotLattice::reset ()
{
  // prepare GNUPLOT
  gp.reset_plot (); // next plot will erase previous ones
  gp.unset_legend ();
  
  gp.cmd ("set palette model HSV");
  gp.cmd ("set palette defined ( 0 0.8333 0 1, 1 0.8333 1 1, 6 0 1 1, 7 0 1 0)");
  gp.cmd ("set style rectangle fillstyle noborder");
  
  // We call this function to forze syncronization of program and gnuplot
  gp.catch_output ();
}    

void
CPlotLattice::set (std::vector<CPlotLattice::index_type> xrange, std::vector<CPlotLattice::index_type> yrange,
		   size_t in_colors)
{
  // update ranges if vectors are not of size null, else respect current ones
  if (xrange.size () != 0)
    {
      switch (xrange.size ())
	{
	case 1 : xmax = xrange.at(0); break; // respect inferior value
	case 2 : xmin = xrange.at(0); xmax = xrange.at(1); break;
	default : throw std::out_of_range("xrange must be a 0, 1 or 2-dim vector.");
	};
      gp.set_xrange (xmin, xmax);
    }
  
  if (yrange.size () != 0)
    {
      switch (yrange.size ())
	{
	case 1 : ymax = yrange.at(0); break; // respect inferior value
	case 2 : ymin = yrange.at(0); ymax = yrange.at(1); break;
	default : throw std::out_of_range("yrange must be a 0, 1 or 2-dim vector.");
	};
      gp.set_yrange (ymin, ymax);
    }
      // set number of colors, respect current if n == 0
  if (in_colors != 0)
    {
      n_colors = in_colors;
      
      // set the values of the pallete
      std::ostringstream cmdstr1;
      cmdstr1 << "set palette maxcolors " << (n_colors + 1);
      gp.cmd(cmdstr1.str());
	  
      std::ostringstream cmdstr2;
      cmdstr2 << "set cbrange [-1:" << (n_colors -1) << "]";
      gp.cmd(cmdstr2.str());
    }
}

void
CPlotLattice::set (size_t n)
{
  std::vector<CPlotLattice::index_type> rx(0), ry(0);
  set(rx, ry, n);
}

void
CPlotLattice::set (CPlotLattice::index_type ixmax, CPlotLattice::index_type iymax, size_t n)
{
  std::vector<CPlotLattice::index_type> rx(1, ixmax), ry(1, iymax); 
  set(rx, ry, n);
}


// update a color of the plot, but only plot if auto_refresh == true

void
CPlotLattice::node2plot (CPlotLattice::index_type index1, CPlotLattice::index_type index2, size_t color)
{
  // NOT NEEDED:
  // x,y belongs to [0,board_side-1]
  // Object indexes will belong to [1,board_side*board_side]

  std::ostringstream cmdstr;

  cmdstr << "set object "
    // map 2 coordinates to a unique index (NOT UNTIL NEEDED)
    // << (y-1)*board_side + x
    //  define left-bottom
	 << " rect from " << index1 << "," << index2
    //  and right-top corners
	 << " to " << index1 + 1 << "," << index2 + 1 
    // choose the color
         << " fc palette cb " << color;

    // unique patterns from 1 to 7
    //  << "fs pattern ", class / (n_colors+1) + 3


  gp.cmd(cmdstr.str());
  //cmdstr << "\n"; std::cout << cmdstr.str();
  
  if (auto_refresh) plot ();
}

/*
// update a color of the plot, but only plot if auto_refresh == true
// it expects that NODE_TYPE accepts operator []
template <typename NODE_TYPE>
void
CPlotLattice::node2plot (const NODE_TYPE & node, size_t color)
{
  node2plot (node[0], node[1], color);
}
*/

// update plotting every change passed to gnuplot
void
CPlotLattice::plot ()
{
  gp.cmd ("plot 0 lc palette cb 0");  
  gp.wait_gp_is_ready ();
}
