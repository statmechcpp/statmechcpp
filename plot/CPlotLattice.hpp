#ifndef CPLOTLATTICE_H
#define CPLOTLATTICE_H

#include <cstdlib>
#include <vector>
#include <cassert>
#include <stdexcept>
#include "gnuplot_i.hpp"

class CPlotLattice
{
public:
  typedef long int index_type; 
private:
  Gnuplot gp;

  size_t n_colors;       // number of different colors to be used
  index_type xmin, xmax; // range to be plotted...
  index_type ymin, ymax; // ...values are 0 by default
  bool auto_refresh;     // read set_auto_refresh

  CPlotLattice (const CPlotLattice &) {}
  CPlotLattice & operator= (const CPlotLattice &) {return (*this);}

public:

  // If true, every node is updated automatically (default)
  // Else, you have to call plot() for watching the changes
  //   This is useful when you don't want to spend time watching some transition
  void set_auto_refresh (bool new_state) {auto_refresh = new_state;}
  bool is_auto_refresh () const {return auto_refresh;}
  index_type get_xmin () const {return xmin;}
  index_type get_xmax () const {return xmax;}
  index_type get_ymin () const {return ymin;}
  index_type get_ymax () const {return ymax;}

  // Indicates if you really want to plot or not
  // (useful if you have to define an instance of this class in your own one)
  CPlotLattice (bool do_plot = false); // non-disturbing-by-default philosophy

  // Prepare the gnuplot system, forgetting any previous message
  // It annulates the effect of calling the constructor with a false argument
  void reset (); 

  // Change the number of colors of the pallete
  void set (size_t in_colors = 0); // 0 means: don't change current value

  // Change the max value of the range, without changing the min value
  void set (index_type ixmax, index_type iymax, size_t in_colors = 0);

  // Change the range to plot:
  //  . if vectors are of size==0, don't change
  //  . if                size==1, change only max value 
  void set (std::vector<index_type> xrange, std::vector<index_type> yrange, size_t in_colors = 0);

  // Drawing (only plot if auto_refresh == true)
  //----------------------------------------------
  void node2plot (index_type x, index_type y, size_t color); 
  // If you have the coordinates in some container which accepts operator[],
  // you can send it directly
  template <typename NODE_TYPE>
  void node2plot (const NODE_TYPE & node, size_t color) {node2plot (node[0], node[1], color);}
  // plot every non updated change passed to node2plot 
  void plot ();
};


#endif
