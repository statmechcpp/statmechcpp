#ifndef KERNELS_CKERNELEQUIPROBABLE_H
#define KERNELS_CKERNELEQUIPROBABLE_H

/*
 * CKernelEquiprobable<int dimension,
 *                     int size,
 *                     typename COORD_TYPE>
 * Usage:
 *
 * The same that CKernelFinite.
 *
 * However, it is intended to be used with kernels for which all valid
 * relative positions have the same probability of being chosen,
 * independently of everything else, such as what is inside that node, time,
 * ect..
 *  
 * It is also expected that you are interested in DERIVATE this class.
 *
 * Description:
 *
 * The same that CKernelFinite.
 *
 * Aditionally, the following functionalities can be used:
 *
 *   random_kernel_entry () : return a random valid kernel entry
 *                            according to a uniform distribution
 *                            (not as some user-specified type
 *                             but boost::array<COORD_TYPE, dim>)
 *
 */

#include "CKernelFinite.hpp"
#include "../gsl/CGSLRng.hpp"

template <unsigned short int dim, size_t size, typename COORD_TYPE>
class CKernelEquiprobable : public CKernelFinite<dim, size, COORD_TYPE>
{
public:
  typedef typename CKernelFinite<dim, size, COORD_TYPE>::kernel_entry_type
  kernel_entry_type;
  typedef typename CKernelFinite<dim, size, COORD_TYPE>::kernel_type kernel_type;

private:
  CGSLRng rng; // The generator of random numbers

public:
  CKernelEquiprobable (const kernel_type & ik)
    : CKernelFinite<dim, size, COORD_TYPE>::CKernelFinite (ik),
      rng()
  {}

  // returns a random valid position of the kernel
  kernel_entry_type random_kernel_entry () const
  {
    return (*this)[rng.uniform_int (size)];
  }
};


#endif
