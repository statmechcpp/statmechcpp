#ifndef KERNELS_CKERNELBORILE4_HPP
#define KERNELS_CKERNELBORILE4_HPP

#include <iostream>
#include "../gsl/CGSLRng.hpp"
#include "nearest_neighbors.hpp"

template <typename LATTICE_TYPE>
class CKernelBorile4
{
public:
  typedef typename LATTICE_TYPE::value_type value_type;
  typedef typename LATTICE_TYPE::index index;
  typedef typename LATTICE_TYPE::node_type node_type;
  typedef boost::array<node_type, 4> kernel_type;

  CGSLRng rng;
  const kernel_type bulk_k;

  const LATTICE_TYPE & lattice; // reference to the lattice 
  const index x_side;
  const index y_side;

  const double eps;
  const value_type prefered_sigma_at_right;
  const value_type prefered_sigma_at_left;
  
  CKernelBorile4 (const LATTICE_TYPE & ilattice, double e,
		   value_type pref_right = 0, value_type pref_left = 1)
    : bulk_k(nearest_neighbors::nn_2D_4_bulk<index> ()),
      lattice(ilattice),
      x_side(ilattice.dim1 ()),
      y_side(ilattice.dim2 ()),
      eps(e),
      prefered_sigma_at_right(pref_right),
      prefered_sigma_at_left(pref_left)
  {}

  // call this function only from the border
  double flip_probab4closed_border (value_type tau, value_type sigma,
				    unsigned int n_diff) const
  {
    const double total_nn = 3.;                    // # of NN in closed border
    const double x = ((double) n_diff) / total_nn; // fraction of different NN

    if (tau == sigma)
      return (1.-eps)*x/((1.-eps)*x+(1.+eps)*(1.-x));
    else
      return (1.+eps)*x/((1.+eps)*x+(1.-eps)*(1.-x));
  }

  // return the kernel decission on the next value for node
  value_type random_value (const node_type & node) const;

};

template <typename LATTICE_TYPE>
typename CKernelBorile4<LATTICE_TYPE>::value_type
CKernelBorile4<LATTICE_TYPE>::random_value (const node_type & node) const
{
  using namespace nearest_neighbors;

  // if node is in the bulk, it is modified to the neighbor node
  // from which value was taken
  const index x_pos = node[0], y_pos = node[1];
  
  // fork behaviour depending on position
  const index right_wall = x_side - 1, left_wall = 0;
  if (left_wall != x_pos && right_wall != x_pos)
    {
      // find a raw relative neighbor according to NN4
      const unsigned long int k_size = bulk_k.size (); // it's 4
      const unsigned long int k_entry_index = rng.uniform_int (k_size);
      const node_type relative_neighbor = bulk_k[k_entry_index];
      
      //  passed node as the one of the neighbor
      const index new_x = x_pos + relative_neighbor[0];
      const index new_y = cyclic_value (y_pos + relative_neighbor[1], y_side);
      
      // return the value of the random neighbor
      return lattice.get_value(new_x, new_y);
    }
  else
    {
      const value_type sigma = lattice.get_value (node); // current value node
      double p; // probability of flipping this node
      
      unsigned int n_diff = 0;
      // DIFFERENT neighbors in Y-direction
      {
	const index cycled_up_pos   = cyclic_value (y_pos + 1, y_side);
	const index cycled_down_pos = cyclic_value (y_pos - 1, y_side);
	if ( sigma != lattice.get_value (x_pos, cycled_up_pos)   ) n_diff++;
	if ( sigma != lattice.get_value (x_pos, cycled_down_pos) ) n_diff++;
      }
      // check remaining neighbor in X-direction
      if (right_wall == x_pos)
	{
	  // right_wall
	  if ( sigma != lattice.get_value(x_pos - 1, y_pos) ) n_diff++;
	  p = flip_probab4closed_border (prefered_sigma_at_right,
					 sigma, n_diff);
	}
      else 
	{
	  // left wall
	  if ( sigma != lattice.get_value(x_pos + 1, y_pos) ) n_diff++;
	  p = flip_probab4closed_border (prefered_sigma_at_left,
					 sigma, n_diff);
	}

      // determine stocastically whether flipping or not
      if (p >= rng.uniform ())
	{ // flip
	  if (sigma == prefered_sigma_at_left) // just a flag,
	    return prefered_sigma_at_right;    // not related with walls
	  else
	    return prefered_sigma_at_left;
	}
      else // no flip
	return sigma;
    } // end of is-node-in-a-lateral-wall?
} //end of member function

#endif
