#ifndef KERNELS_CKERNELNN_2D_4_PBC_HPP
#define KERNELS_CKERNELNN_2D_4_PBC_HPP

/*
 * CKernelNN_2D_4_PBC
 *
 * Description:
 *
 * It is based on CKernelEquiprobablePBC.
 *
 * It implements a Nearest Neighbours kernel for a rectangular lattice such
 * that:
 *
 *  * It is a 2-dimensional lattice.
 *
 *  * There are Periodic Boundary Conditions. Neighbours outside the limits
 *    of the lattice are nodes on the opossite side of the lattice.
 *
 *  * There is no distintion when choosing a node over any other, whenever
 *    they are allowed. So, what time is it, what is inside that node, or
 *    eveything else doesn't matter.
 *
 *  * The only allowed nodes are the 8 surrounding a given one. Which of them
 *    is chosen is always determined by a uniform distribution.
 *
 * Usage:
 *
 * First, construct a instance of this class specifying @COORD_TYPE,
 * the type of the indices you will be dealing with and the size
 * of the lattice.
 *
 *    CKernelNN_2D_4_PBC<int> my_kernel (size1, size2);
 *
 *  or also
 *
 *   CKernelNN_2D_4_PBC<int> my_kernel (sizes);
 *
 *  where @sizes is any container which components are the size
 *  of every dimension of the lattice. operator[] will be used.
 *
 * Then, just call the member function
 *
 *   my_kernel.random_node (node)
 *
 * and it will return a neighbour of @node.
 *
 * WARNING: It is not checked that @node has the proper dimensions or their
 * coordinates are inside the lattice. But provided that, you will always
 * obtain a node inside the lattice (according to PBC).
 *
 */

#include "CKernelEquiprobablePBC.hpp"

template <typename COORD_TYPE>
class CKernelNN_2D_4_PBC : public CKernelEquiprobablePBC<2, 4, COORD_TYPE>
{  
public:
  typedef typename CKernelEquiprobablePBC<2,4,COORD_TYPE>::kernel_entry_type
  kernel_entry_type;
  typedef typename CKernelEquiprobablePBC<2,4,COORD_TYPE>::kernel_type
  kernel_type;

private:
  kernel_entry_type sizes2array (size_t size1, size_t size2)
  {
    kernel_entry_type isizes;
    isizes[0] = size1;
    isizes[1] = size2;
    return isizes;
  }

public:
  // user can pass the size in a NODE_TYPE
  template <typename NODE_TYPE>
  CKernelNN_2D_4_PBC (const NODE_TYPE & sizes) :
    CKernelEquiprobablePBC<2, 4, COORD_TYPE>::CKernelEquiprobablePBC
    (nearest_neighbors::nn_2D_4_bulk<COORD_TYPE> (), sizes)
  {}
  // user can pass each size individually, without a container
  CKernelNN_2D_4_PBC (size_t size1, size_t size2) :
    CKernelEquiprobablePBC<2, 4, COORD_TYPE>::CKernelEquiprobablePBC
    (nearest_neighbors::nn_2D_4_bulk<COORD_TYPE> (),
     sizes2array (size1, size2))
  {}

};

#endif
