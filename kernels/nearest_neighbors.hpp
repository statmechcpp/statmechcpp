#ifndef KERNELS_NEAREST_NEIGHBORS_HPP
#define KERNELS_NEAREST_NEIGHBORS_HPP

#include <boost/array.hpp>

namespace nearest_neighbors
{
  // return the cyclic value of a coordinate,
  // leaving it inside the interval [0, side-1]
  template <typename VALUE_TYPE>
  VALUE_TYPE cyclic_value (VALUE_TYPE c, VALUE_TYPE side);

  // Functions that return representative relative positions of a kernel
  // in the bulk of a lattice, but without any care when boundary
  // conditions apply
  template <typename VALUE_TYPE>
  class nn2D
  {
    const VALUE_TYPE zero;
    const VALUE_TYPE one;

  public:
    typedef typename boost::array<VALUE_TYPE, 2> node_type;
    nn2D () : zero(0), one(1) {}
    node_type r_right ( )     {node_type v; v[0] = + one; v[1] =  zero; return v;}
    node_type r_left ()       {node_type v; v[0] = - one; v[1] =  zero; return v;}
    node_type r_up ()         {node_type v; v[0] =  zero; v[1] = + one; return v;}
    node_type r_down ()       {node_type v; v[0] =  zero; v[1] = - one; return v;}
    node_type r_right_up ()   {node_type v; v[0] = + one; v[1] = + one; return v;}
    node_type r_left_up ()    {node_type v; v[0] = - one; v[1] = + one; return v;}
    node_type r_right_down () {node_type v; v[0] = + one; v[1] = - one; return v;}
    node_type r_left_down ()  {node_type v; v[0] = - one; v[1] = - one; return v;}
  };


  /*
  template <typename VALUE_TYPE>
  boost::array<VALUE_TYPE> nn_1D_2_bulk (void);
  */

  template <typename VALUE_TYPE>
  boost::array<boost::array<VALUE_TYPE, 2>, 4> nn_2D_4_bulk (void);
  
  template <typename VALUE_TYPE>
  boost::array<boost::array<VALUE_TYPE, 2>, 8> nn_2D_8_bulk (void);

  template <typename VALUE_TYPE>
  VALUE_TYPE cyclic_value (VALUE_TYPE c, VALUE_TYPE side)
  {
    return (c >= 0) ? c - side * (c/side) : c - side * ((c+1)/side - 1);
  }

  // Functions that return a boost::array with valid relative
  // positions according to a given kernel in the bulk of a lattice:
  // that is, without indications and singularities of any
  // boundary condition.
  /*
  template <typename VALUE_TYPE>
  boost::array<VALUE_TYPE, 2> nn_1D_2_bulk (void)
  {
    boost::array<VALUE_TYPE, 2> k;
    k[0] = +1; // right
    k[1] = -1; // left
    return k;
  }
  */
  template <typename VALUE_TYPE>
  boost::array<boost::array<VALUE_TYPE, 2>, 4> nn_2D_4_bulk (void)
  {
    nn2D<VALUE_TYPE> a;
    boost::array<typename nn2D<VALUE_TYPE>::node_type, 4> k;
    
    k[0] = a.r_right ();
    k[1] = a.r_left ();
    k[2] = a.r_up ();
    k[3] = a.r_down ();
    return k;
  }

  template <typename VALUE_TYPE>
  boost::array<boost::array<VALUE_TYPE, 2>, 8> nn_2D_8_bulk ()
  {
    nn2D<VALUE_TYPE> a;
    boost::array<typename nn2D<VALUE_TYPE>::node_type, 8> k;
    k[0] = a.r_right ();
    k[1] = a.r_left ();
    k[2] = a.r_up ();
    k[3] = a.r_down ();
    k[4] = a.r_right_up ();
    k[5] = a.r_right_down ();
    k[6] = a.r_left_up ();
    k[7] = a.r_left_down ();
    return k;
  }
}

#endif



