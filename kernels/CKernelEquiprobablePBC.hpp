#ifndef KERNELS_CKERNELEQUIPROBABLEPBC_HPP
#define KERNELS_CKERNELEQUIPROBABLEPBC_HPP

/*
 * CKernelEquiprobableInf<int dimension,
 *                        int size,
 *                        typename COORD_TYPE>
 *
 * Usage:
 *
 * The same that CKernelEquiprobable, but it is intended to be used with
 * kernels for which:
 *
 *  * There are Peridic Boundary Conditions (PBC). That is, opposite lattice
 *    borders are connected topologically.
 *
 *  * Only relative positions matters, so constructor should be called 
 *    with elements containing valid relative positions in the bulk
 *    (don't worry about the borders, it is done here)
 *
 * Description:
 *
 * The same that CKernelEquiprobable.
 *
 * Adit)ionally, the following functionalities can be used:
 *
 *   random_node (node) : return a random valid node (absolute position)
 *                        using @node as reference. More concisely,
 *                        it adds to @node the relative
 *                        position contained inside a kernel entry chosen
 *                        according to a uniform distribution.
 *                        Then, it returns the PBC-equivalent node according
 *                        to the sizes passed to the ctor.
 *
 *                        There are two version of this function: a returning
 *                        one and a overwriting one.
 *
 */

#include "CKernelEquiprobable.hpp"
#include "nearest_neighbors.hpp"

template <unsigned short int dim, size_t size, typename COORD_TYPE>
class CKernelEquiprobablePBC :
  public CKernelEquiprobable<dim, size, COORD_TYPE>
{
public:
  typedef typename CKernelEquiprobable<dim,size,COORD_TYPE>::kernel_entry_type
  kernel_entry_type;
  typedef typename CKernelEquiprobable<dim,size,COORD_TYPE>::kernel_type
  kernel_type;

private:
  // array containing widths of the lattice for each dimension
  const kernel_entry_type sizes; 

public:

  // Ctor used when widths are passed in the standard format
  // in particular, it should be used by a derived class
  // when they takes the widths individually and it has to create
  // its own container
  CKernelEquiprobablePBC (const kernel_type & ik,
			  const kernel_entry_type & isizes) :
    CKernelEquiprobable<dim,size,COORD_TYPE>::CKernelEquiprobable (ik),
    sizes(isizes)
  {}

  // Ctor in a useful form for the final user
  // (it takes a NODE_TYPE instance with the limits of the system)
  // NODE_TYPE must have an operator[]
  template <typename NODE_TYPE>
  CKernelEquiprobablePBC (const kernel_type & ik, 
			  const NODE_TYPE & isizes) :
    CKernelEquiprobable<dim, size, COORD_TYPE>::CKernelEquiprobable (ik),
    sizes(dim)
  {
      for (size_t i = 0; i != dim; i++)
	sizes[i] = isizes[i];
  }

  // random_node gives you a node calculated by
  // adding a random kernel entry to the node passed
  // option 1: return a new node
  template <typename NODE_TYPE>
  NODE_TYPE random_node (const NODE_TYPE &) const;
  // option 2: overwrite the node
  template <typename NODE_TYPE>
  void random_node (NODE_TYPE *) const;

};

//option 1:
template <unsigned short int dim, size_t size, typename COORD_TYPE>
template <typename NODE_TYPE>
NODE_TYPE
CKernelEquiprobablePBC<dim, size, COORD_TYPE>::random_node (const NODE_TYPE & node) const
{
  // take a valid offset for the bulk
  // rng.uniform_size_t (size) returns an integer in [0,size-1]
  const kernel_entry_type offset
    = CKernelEquiprobable<dim,size,COORD_TYPE>::random_kernel_entry ();
  // create a new node identical to the one defined by the user
  // we don't need to know the constructor of that type,
  // we rely on the copy-constructor
  NODE_TYPE new_node(node);
  
  // loop over node's components
  for (size_t i = 0; i != dim; i++)
    new_node[i] =
      nearest_neighbors::cyclic_value (new_node[i] + offset[i],
					sizes[i]);
  
  return new_node;
}
// option 2:
template <unsigned short int dim, size_t size, typename COORD_TYPE>
template <typename NODE_TYPE>
void
CKernelEquiprobablePBC<dim, size, COORD_TYPE>::random_node (NODE_TYPE * pnode) const
{
  // take a valid offset for the bulk
  // rng.uniform_size_t (size) returns an integer in [0,size-1]
  const kernel_entry_type offset
    = CKernelEquiprobable<dim,size,COORD_TYPE>::random_kernel_entry ();
  
  // loop over node's components
  for (size_t i = 0; i != dim; i++)
    (*pnode)[i] =
      nearest_neighbors::cyclic_value ((*pnode)[i] + offset[i],
					sizes[i]);
}


#endif
