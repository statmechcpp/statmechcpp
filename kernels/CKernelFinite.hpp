#ifndef KERNELS_CKERNELFINITE_H
#define KERNELS_CKERNELFINITE_H

/*
 * CKernelFinite<int dimension,
 *               int size,
 *               typename COORD_TYPE>
 *
 * Usage:
 *
 * It is intended to be used for accesing, while keeping inmutable, the
 * valid relative positions that a kernel can use, provided that only a
 * finite number of positions are allowed by the kernel.
 *
 * It is also expected that you are interested in DERIVATE this class.
 *
 * Description:
 *
 * An instance of this object will keep a container of size @size
 * keeping the considered nodes, which elements are coordinates of type
 * COORD_TYPE. Nodes must be of type boost::array<COORD_TYPE, dim>.
 *
 * The particular values that the container will contain
 * will be passed to the ctor as a boost::array of size @size.
 *
 * The available functionalities are:
 *
 *   operator[]    : read-ONLY access to that elements
 *   dimension ()  : return @dimension (the template parameter)
 *
 */

#include <cstdlib>
#include <stdexcept>
#include <boost/array.hpp>

template <unsigned short int dim, size_t size, typename COORD_TYPE>
class CKernelFinite
{
public:
  typedef typename boost::array<COORD_TYPE, dim> kernel_entry_type;
  typedef typename boost::array<kernel_entry_type, size> kernel_type;
private:
  // container for valid relative positions
  const kernel_type k;

public:
  unsigned short int dimension () const {return dim;}
  size_t kernel_entries () const {return size;}
  
  CKernelFinite (const kernel_type & ik) : k(ik) {}
      
  // User has no permission to modify the array
  const kernel_entry_type & operator[] (size_t n) const {return k[n];}
};

#endif
