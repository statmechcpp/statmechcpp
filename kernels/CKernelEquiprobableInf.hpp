#ifndef KERNELS_CKERNELEQUIPROBABLEINF_HPP
#define KERNELS_CKERNELEQUIPROBABLEINF_HPP

/*
 * CKernelEquiprobableInf<int dimension,
 *                        int size,
 *                        typename COORD_TYPE>
 * Usage:
 *
 * The same that CKernelEquiprobable, but it is intended to be used with
 *  kernels for which:
 *
 *  * There are no boundary conditions (maybe because they are protected by
 *    an external check or you are really dealing with an infinite system)
 *
 *  * Only relative positions matters, so constructor should be called 
 *    with elements containing valid relative positions.
 *
 * Description:
 *
 * The same that CKernelEquiprobable.
 *
 * Aditionally, the following functionalities can be used:
 *
 *   random_node (node) : return a random valid node (absolute position)
 *                        using @node as reference. More concisely,
 *                        it returns the result of adding to @node the relative
 *                        position contained inside a kernel entry chosen
 *                        according to a uniform distribution.
 *
 *                        There are two version of this function: a returning
 *                        one and a overwriting one.
 */

#include "CKernelEquiprobable.hpp"

template <unsigned short int dim, size_t size, typename COORD_TYPE>
class CKernelEquiprobableInf :
  public CKernelEquiprobable<dim, size, COORD_TYPE>
{
public:
  typedef typename CKernelEquiprobable<dim,size,COORD_TYPE>::kernel_entry_type
  kernel_entry_type;
  typedef typename CKernelEquiprobable<dim,size,COORD_TYPE>::kernel_type
  kernel_type;

  CKernelEquiprobableInf (const kernel_type & ik) :
    CKernelEquiprobable<dim,size,COORD_TYPE>::CKernelEquiprobable (ik)
  {}

  // random_node (node) gives you a node calculated adding an offset to
  // @node a random entry of the available kernel's relative positions
  // option 1: return a new node
  template <typename NODE_TYPE>
  NODE_TYPE random_node (const NODE_TYPE &) const;
  // option 2: overwrite node (we forze a pointer-style to remember the user
  //           we are overwriting the node
  template <typename NODE_TYPE>
  void random_node (NODE_TYPE *) const;
};


template <unsigned short int dim, size_t size, typename COORD_TYPE>
template <typename NODE_TYPE>
NODE_TYPE
CKernelEquiprobableInf<dim,size,COORD_TYPE>::random_node (const NODE_TYPE & node) const
{
  // take a valid offset from the kernel
  const kernel_entry_type offset
    = CKernelEquiprobable<dim,size,COORD_TYPE>::random_kernel_entry ();
  // create a new node identical to the one defined by the user
  // we don't need to know the constructor of that type,
  // we rely on the copy-constructor
  NODE_TYPE new_node(node);
  
  // loop over node's components
  for (size_t i = 0; i != dim; i++)
    new_node[i] += offset[i];
  
  return new_node;
}

template <unsigned short int dim, size_t size, typename COORD_TYPE>
template <typename NODE_TYPE>
void
CKernelEquiprobableInf<dim,size,COORD_TYPE>::random_node (NODE_TYPE * pnode) const
{
  // ask to the kernel a valid offset
  const kernel_entry_type offset
    = CKernelEquiprobable<dim,size,COORD_TYPE>::random_kernel_entry ();
  
  // loop over node's components
  for (size_t i = 0; i != dim; i++)
    (*pnode)[i] += offset[i];
}


#endif
