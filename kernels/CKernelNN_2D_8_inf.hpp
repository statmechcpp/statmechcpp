#ifndef KERNELS_CKERNELNN_2D_8_INF_HPP
#define KERNELS_CKERNELNN_2D_8_INF_HPP

/*
 * CKernelNN_2D_8_inf<typename COORD_TYPE>
 *
 * Description:
 *
 * It is based on CKernelEquiprobableInf.
 *
 * It implements a Nearest Neighbours kernel for a rectangular lattice such
 * that:
 *
 *  * It is a 2-dimensional lattice.
 *
 *  * There are no boundary conditions (maybe because they are protected by
 *    an external check or you are really dealing with an infinite system)
 *
 *  * There is no distintion when choosing a node over any other, whenever
 *    they are allowed. So, what time is it, what is inside that node, or
 *    eveything else doesn't matter.
 *
 *  * The only allowed nodes are the 8 surrounding a given one. Which of them
 *    is chosen is always determined by a uniform distribution.
 *
 * Usage:
 *
 * First, construct a instance of this class specifying @NODE_TYPE,
 * the type of the nodes you will be dealing with.
 *
 * A node is an object which contains coordinates which indicates its position
 * on a rectangular lattice. @NODE_TYPE can be any type, but operator[] must
 * be defined for accessing the coordinate of each dimension (from 0 to d-1,
 * being d the number of dimensions).
 *
 *   CKernelNN_2D_8_inf<int> my_kernel;
 *
 * Then, just call the member function
 *
 *   my_kernel.random_node (node)
 *
 * and it will return a neighbour of @node.
 *
 * WARNING: It is not checked that @node has the proper dimensions or their
 * coordinates are reasonable.
 *
 * WARNING: In particular, you should be aware when using this kernel in finite
 * lattices, since random_node (node) could return a node outside the lattice.
 *
 */

#include "CKernelEquiprobableInf.hpp"
#include "nearest_neighbors.hpp"

template <typename COORD_TYPE>
class CKernelNN_2D_8_inf : public CKernelEquiprobableInf<2,8,COORD_TYPE>
{  
public:
  CKernelNN_2D_8_inf () :
    CKernelEquiprobableInf<2, 8, COORD_TYPE>::CKernelEquiprobableInf
    (nearest_neighbors::nn_2D_8_bulk<COORD_TYPE> ())
  {}
};

#endif
