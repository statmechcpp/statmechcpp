#ifndef CRSA_H
#define CRSA_H

#include "../gsl/CLogHisto.hpp"
#include "../gsl/CGSLHisto.hpp"

class CRSA : public CLogHisto<CGSLHisto>
//class CRSA : public CGSLHisto
{
public : 
  const size_t side; // sqrt root of the max number of individuals a species can haves
                     // can be seen as the side of a square lattice where it is measured

    CRSA (size_t iside) : CLogHisto<CGSLHisto>(1, iside*iside, 400, true), side(iside) {}
  //    CRSA (size_t iside) : CGSLHisto(1, iside*iside, iside*iside-1), side(iside) {}
};

#endif
