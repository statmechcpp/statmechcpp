#include "CCladogram.hpp"

/*
 * find_and_set_species_id (index)
 *
 * Requisites:
 *
 * THIS FUNCTION NEEDS that the cladogram have been previously pre-processed
 * so the species_id field of the individuals which had associated the walkers
 * where the rest of individuals of the same species arrived recursively,
 * has a species_id != clad.size ().
 *
 * In practice, it is done in the first part of the function 'set', just
 * before this function is called.
 *
 * Output:
 *
 * This is a recursive function which returns the species_id of a given
 * cladogram's entry (provided its index). However, it should only be used
 * while constructing the cladogram, since it is its only purpose.
 *
 * Utility / Side-effects:
 *
 * When this function is called with @index, clad[index].species_id is
 * set with the species_id of its species by looking up recursively in the
 * cladogram.
 *
 * ALSO, this function sets the species_id of all the individuals whose
 * associated walkers are in the genealogical path between @index and the first
 * ancestor of its species (a), but not those individuals of the same species
 * which had associated walkers which coalesced into @index (b), nor those
 * which are on a different sub-branch (c). Indivudals which match cases b and
 * c will need that function be called for other indexes for being set.
 *
 *
 *                                         -----------------o NOT SOLVED (b)
 *                                        /
 *                               --------------o Walker associated to @index
 *                              / 
 *                       -------------------------o               SOLVED (a)
 *                      /   \
 *                     /     -----------------------------o   NOT SOLVED (c)
 *                    /
 * o-----------------------------------o   Walker which reached 1st ancestor 
 *
 * If the function is called for all the indexes, all species_id will be set.
 *
 */
size_t
CCladogram::find_and_set_species_id (size_t index)
{
  if (clad[index].species_id != clad.size ())
    return clad[index].species_id;
  else
    {
      const size_t relative_index = clad[index].main_relative_index;
      // NOTE:
      // clad was prepared in set () such a way that:
      // If you are not the first ancestor, your main_relative_index has sense.
      //
      // That is:
      //
      // IF   clad[index].species_id       == size (species_id is unknown)
      // THEN clad[index].main_relative_id != size (main_relative_id points)
      const size_t species = find_and_set_species_id (relative_index);
      clad[index].species_id = species;
      return species;
    }
}

CRSA
CCladogram::alloc_RSA (size_t side) const
{
  CRSA rsa(side);
  return rsa;
}

void
CCladogram::measure_RSA (CRSA & rsa) const
{
  const size_t n_nodes = rsa.side*rsa.side;

  if (lattice_side () < rsa.side)
    throw std::invalid_argument("CCladogram has not got so many entries!");
  
  // define and initialize individuals
  // (number of individuals of a given specie)
  std::vector<size_t> individuals(n_nodes, 0); 

  // fill @individuals
  for (size_t index = 0; index < n_nodes; index++)
    (individuals.at(clad[index].species_id))++;

  // increase RSA according to @individuals
  for (size_t index = 0; index < n_nodes; index++)
    rsa.increment (individuals.at(index));
}

/*
void 
CCladogram::measure_multiple_RSA (...) const

void
CCladogram::measure_SAR (CSAR & histo) const
{

}
*/
