#ifndef CWALKERTABLE_H
#define CWALKERTABLE_H

/*
 * Class for managing the list of Walkers needed to implement the
 *   dual representation of the voter model. We call this list a "table" (WT).
 *
 * The WT should be used for keeping the state of @side^dim walkers on a 
 * regular lattice and anotating the walkers which coalesced into others.
 * Once that all parental information be assigned to every walker, the WT
 * can be parsed to construct a cladogram by CCladogram.
 *
 * When initializing the WT, it locates the walkers in different nodes of the
 * hiper-cube of side @side with a corner on (0,...,0) and other on
 * (@side,...@side).
 *
 * Usually, you'll refer the walker by the current index on the WT, which go
 * from 0 to @side^dim - 1. However the index of any walker can change always
 * you discard some walker of the table, so YOU SHOULD NOT relay on your
 * previous information after discarding.
 *
 * You can ask a lot of information about the walker of the index you wish.
 *   Aditionally, you can change part of its state:
 *
 *   . You can discard (different to erase, since all the info will be saved)
 *       a walker of the list. If you do that:
 *     . The number of alive walkers will be reduced by 1.
 *     . YOU SHOULD NOT modify the state of a walker wich index is equal or
 *         higher than the current number of alive walkers.
 *     . You can know the number of alive walkers using the function
 *         'alive_walkers'.
 *
 *   . You can move a walker to ALMOST any position (you may use the help of
 *       a kernel)
 *      . That includes positions far far away from the original hipercube (!).
 *     However, YOU SHOULD NOT place two walkers in the same position.
 *      . This rule does not apply if the walker was discarded before, so it
 *           is considered dead now.
 *      . You can easily check if there is some alive walker in a given
 *           position with function 'id_of_who_is_in'.
 *
 *   . You can stablish the id of the walker which coalesced into other with
 *       function 'set_coalesced_into'.
 *
 * When constructed,
 *
 *   CWalkerTable<NODE_TYPE, dim> wt(side);
 *
 * the WT is NOT functional. If you want to initialize it call
 *
 *    wt.reset ();
 *
 * (all the previous info on it will be erased). The final user is going
 * to use CCoalescent class and not this, so it is not so important
 * to not initialize when constructing.
 *
 * Initially, all the walkers (of class CWalker) are placed into a container
 * of type @NODE_TYPE and are initialized:
 *
 *   . With a different id, from 0 to @side^dim -1
 *   . The coalesced_into field of each walker has id @side^dim (a flag)
 *   . All the walkers are placed in a initial position which is correlated to
 *       its id. So, if you know the id, you can calculate in which position it
 *       was originally. This information is facilitated by 'iterate_position'.
 *   . Birth_date ?? (reasonable would be 1, instead of 0)
 */

#include <iostream>
#include <cstdlib>
#include <stdexcept>
#include <vector>
#include <boost/unordered_map.hpp>
#include <boost/version.hpp>
#include "CWalker.hpp"

// Your boost version could not include hash_value (boost::array)
#if BOOST_VERSION < 105000
namespace boost
{
  template<class T, std::size_t N>
  std::size_t hash_value (const boost::array<T,N> & arr)
  {
    return boost::hash_range (arr.begin (), arr.end ());
  }
}
#endif

template <unsigned short int dim>
class CWalkerTable
{
public:
  typedef boost::array<long int,dim> pos_type;
  typedef std::vector<CWalker<dim> > table_type;
  typedef boost::unordered_map<pos_type,size_t> umap_type;

private:
  const size_t lattice_side_val;
  const size_t capacity_val;
  size_t current_size;
  table_type wt;
  umap_type node_map; // auxiliar look-up table

  void just_swap (size_t index1, size_t index2);

public:
  // Return number of walkers allowed in a lattice give side 
  size_t calc_capacity (size_t iside) const; // iside^dim

  CWalkerTable (size_t iside)
    : lattice_side_val(iside),
      capacity_val (calc_capacity (iside)),
      current_size (calc_capacity (iside)),
      wt (calc_capacity (iside)),
      node_map (calc_capacity (iside))
  {}

  // Reading functions
  size_t lattice_side () const {return lattice_side_val;}
  size_t total_walkers () const {return capacity_val;}
  size_t alive_walkers () const {return current_size;}
  size_t get_id (size_t index) const {return (wt.at(index)).get_id ();}
  size_t get_coalesced_into (size_t index) const {return (wt.at(index)).get_coalesced_into ();}
  pos_type get_pos (size_t index) const {return (wt[index]).get_pos ();}

  size_t id_of_who_is_in (const pos_type & node) const;

  // Function used for positioning the walkers on a lattice in order.
  // The chosen order is very important since it is expected by CCladogram.
  // It avoids (sometimes) to remap the walkers on a lattice for taking
  //   spatial measures)
  void iterate_position (size_t & x, size_t & y) const;

  // Writing functions

  // clean and initialize the WT
  void reset ();
  // discard entry from WT
  void discard (size_t index2discard, pos_type node2discard);
  void set_pos (size_t index, pos_type node);
  // WARNING: second argument is the ID of permanent walker, NO its index
  void set_coalesced_into (size_t coalesced_index, size_t passive_id) {wt.at(coalesced_index).set_coalesced_into (passive_id);}


};

template <unsigned short int dim>
size_t
CWalkerTable<dim>::calc_capacity (size_t iside) const
{
  size_t elements = 1;
  for (short int idim = 1; idim <= dim; idim++)
    elements *= iside;
  return elements;
}


/*
 ***********************************************************
 * Trick for positioning the walkers in the prefered order
 ***********************************************************
 *
 * iterate_position: regardless of the size of the system
 * or any other variable, this function MODIFIES the passed
 * arguments following the next order
.            .  .
| 9 <- 8 <- 7   .
|           ^   ^
| 4 <- 3    6   11
|      ^    ^   ^
| 1 -> 2    5   10      (numbers are id's)
|------------------ 
*
* First value passed as input should be position (0,0) so it will change it to
* (0,1) and the last one should be (1, side-1) so you'll get (0,side-1).
*/
template <unsigned short int dim>
void
CWalkerTable<dim>::iterate_position (size_t & x, size_t & y) const
{
  if (y < x) // frequently it will be true, so it is well placed (efficiency)
    y++;     // it does not match case: x == y == 0
  else
    {
      if (0 != x) x--;
      // x == 0  means, cycle is finished
      // it matches the case: x == y == 0
      else
	{
	  x = y + 1;
	  y = 0;
	}
    }
}


/*
 * NOTE: Walkers will be moved all the time,
 *       so indexes will not identify walkers
 *       or equivalently, they will not identify their initial position);
 *       that is accomplished by the id
 */
template <unsigned short int dim>
void
CWalkerTable<dim>::reset ()
{
  // set to max the size of the table
  current_size = total_walkers ();
  // start with a void umap
  node_map.clear ();

  // init table.
  if (dim == 2)
    {
      size_t x = 0, y = 0;
      for (size_t index = 0; index < total_walkers (); index++)
	{
	  const size_t id = index; // id's must start from 0 (trivial, I know)

	  wt.at(index).set_id (id);
	  wt.at(index).set_coalesced_into (total_walkers ()); // flag
	  // place walker at initial position
	  // access directly to position, avoiding construct a NODE_TYPE
	  wt.at(index).current_pos.at(0) = x;
	  wt.at(index).current_pos.at(1) = y;
	  // build an entry in node_map
	  node_map.emplace (wt.at(index).get_pos (), index);

#ifdef DEBUG
	  // print some info about walkers in WT
	  std::cout
	    << "Walker " << id
	    << ": (" << x << "," << y << ")"
	    << std::endl;
#endif

	  // change position for the next iteration
	  // (last position set on the loop will not be used: (capacity (),0))
	  iterate_position (x, y);
	}
    }
  else
    {
      throw std::invalid_argument("reset () is only coded in 2D yet.");
    }
}


/*
 * id_of_who_is_in:
 * it returns the id of the walker in @node, or total_walkers () if there is no walker there
 */
template <unsigned short int dim>
size_t
CWalkerTable<dim>::id_of_who_is_in (const pos_type & node) const
{

  typename CWalkerTable<dim>::umap_type::const_iterator got = node_map.find (node);

#ifdef DEBUG
  std::cout << "Who is in (" << node.at(0) << ", " << node.at(1) << ")? ";
#endif

  
  if ( node_map.end () == got )
    {
#ifdef DEBUG
      std::cout << "No one!" << std::endl;
#endif
      return (total_walkers ());
    }
  else
    {
#ifdef DEBUG
      std::cout << "Walker " << (wt.at(got->second)).get_id ()
		<< " (index = " << got->second << ")."
		<< std::endl;
#endif
      // got points to a pair (key, index of table)
      return ((wt.at(got->second)).get_id ());
    }
}

template <unsigned short int dim>
void
CWalkerTable<dim>::set_pos (size_t index, pos_type node)
{
  // update info on node_map
  node_map.erase (wt[index].get_pos ());
  node_map.emplace (node, index);
  // change WT
  wt[index].set_pos (node);
}
/*
 * just_swap:
 * swap two entries of WalkerTable and don't worry about anything
 */
template <unsigned short int dim>
void
CWalkerTable<dim>::just_swap (size_t index1, size_t index2)
{
  const CWalker<dim> flag = wt[index1];
  wt[index1] = wt[index2];
  wt[index2] = flag;
}

/*
 * discard:
 *
 * 1. move away (but don't delete) @wt[index]
 * 2. correct all internal info involved
 *
 * NOTE about perfomance:
 *   This function asks the node to erase on NodeMap as an argument
 *   instead of looking for it one more time.
 */
template <unsigned short int dim>
void
CWalkerTable<dim>::discard (size_t index2discard, pos_type node2discard)
{
  /* 
   * decrease number of valid walkers (@current_n).
   * @current_n's walker will be out of bounds because of that;
   * since we want exactly that for @index's:
   * SWAP them!
   */
  if (index2discard < current_size)
    {
#ifdef DEBUG
      std::cout << "Walker " <<  wt.at(index2discard).get_id ()
		<< " is being discarded... Now"
		<< " Walker " <<  wt.at(current_size-1).get_id ()
	        << " (index = " << current_size-1 << ")"
		<< " is going to occupy its index: " << index2discard
		<< std::endl;
#endif
      just_swap (index2discard, --current_size);

      // correct info of valid innocent walker on NodeMap
      const pos_type innocent_walker_pos = wt[index2discard].get_pos ();
#ifdef DEBUG
      std::cout << "After swapping,"
		<< " Walker " << wt.at(index2discard).get_id ()
	        << " has (index = " << index2discard << ")."
		<< " It continues on position ("
		<< innocent_walker_pos[0] << ","
		<< innocent_walker_pos[1] << ")"
	        << " and node map is going to be corrected."
		<< std::endl;
#endif

      node_map.at (innocent_walker_pos) = index2discard;
      // Remove entry of discarded walker on NodeMap (now out of range in wt)
      // node_map.erase (wt[current_n].get_pos ());
      // It can be done directly since the gentle user gives you the node as argument (!)
      node_map.erase (node2discard);
    }
  else
    throw std::out_of_range("discard must take a index lower than alive_walkers ()");    
}
  
#endif

/*
 * Code for filling WalkerTable with walkers ordered by columns in the lattice
 *
  if (dim == 2)
    {
      // init table
      for (size_t i = 0; i < l; i++)
	for (size_t j = 0; j < l; j++)
	  {
	    // convert pair of coordinates of a node in a unique index
	    // for the walker which starts at that node.
	    const size_t index = i*l + j;
	    const size_t id = index + 1; // we want id's starting at 1
	    wt.at(index).set_id (id);
	    wt.at(index).set_parent (0);  // ...since 0 is reserved for unknown id
	    
	    // place walker at initial position
	    // access directly to components so you don't have to construct a NODE_TYPE
	    wt.at(index).current_pos[0] = i;
	    wt.at(index).current_pos[1] = j;

	    // build an entry in node_map
	    node_map.emplace (wt.at(index).position (), id);
	  }
    }
*/
