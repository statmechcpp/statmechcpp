#ifndef CCOALESCENT_H
#define CCOALESCENT_H


#include "../gsl/CGSLRng.hpp"
#include "CCladogram.hpp"

template <typename KERNEL_TYPE, unsigned short int dim>
class CCoalescent
{
  typedef boost::array<long int,dim> pos_type;

  const double rate; // annihilation rate
  const KERNEL_TYPE kernel;
  CGSLRng rng;
  CWalkerTable<dim> wt;      // table of walkers

  // Annihilate a walker given its index and the node where it is
  void annihilate (size_t index, const pos_type & node)
  {wt.discard (index, node);}
  // Coalesce a walker into other given the index and node of the first,
  // the id of the second
  void coalesce (size_t coalesced_index, size_t permanent_id,
		 const pos_type & child_node);
  // reinitialize wt (without dropping allocated mem)
  // and get the state ready for calling 'get_cladogram'
  void run ();

public:
  CCoalescent (double irate, size_t iside);
  // it returns a blank cladogram, but ok for being passed to get_cladogram ()
  CCladogram alloc_cladogram ();
  // run a coalescent process and fill @clad with the results
  void get_cladogram (CCladogram & clad);
};

template <typename KERNEL_TYPE, unsigned short int dim>
CCoalescent<KERNEL_TYPE,dim>::CCoalescent (double irate, size_t iside)
  : rate(irate),
    // the kernel will be probably a simple one without border conditions but,
    // in general, if it needs some constructing parameters, you'll have to
    // specializate this function for dealing with them.
    kernel (),
    rng (),
    wt (iside)    
{}


/*
 * coalesce:
 *
 * 1. mark walker with id @passive_id as the walker where walker with index
 *      @coalesced_index coalesced into.
 *
 * 2. discard walker with index @coalesced_index, which was in @child_node
 *    just "before" coalescence happened (different to the node where 
 *    @passive_id was all the time and where coalescence happened)
 *
 */
template <typename KERNEL_TYPE, unsigned short int dim>
void
CCoalescent<KERNEL_TYPE,dim>::coalesce (size_t coalesced_index, size_t passive_id, const pos_type & child_node)
{
  // annotate permanent walker on walker which coalesced into it
  wt.set_coalesced_into (coalesced_index, passive_id);
  // discard coalesced walker
  wt.discard (coalesced_index, child_node);
}

template <typename KERNEL_TYPE, unsigned short int dim>
void
CCoalescent<KERNEL_TYPE,dim>::run ()
{
  // Fill a lattice of random walkers
  wt.reset ();
  const size_t total_walkers = wt.total_walkers ();
  while (true)
    {
      // How many walkers are still running?
      const size_t alive_walkers = wt.alive_walkers ();
      // Last walker will die by annihilation yes or yes
      if (1 == alive_walkers) break;

      // Choose randomly a walker
      const size_t w1_index = rng.uniform_int (alive_walkers);
      // Where is it?
      const pos_type w1_pos = wt.get_pos (w1_index);

      // The random walker deads with probability rate      
      if (rng.uniform () < rate)
	{
#ifdef DEBUG
	  // print some info about walkers in WT
	  std::cout
	    << "Walker " <<  wt.get_id (w1_index)
	    << " (index = " << w1_index << ") is being annihilated in"
	    << " (" << w1_pos.at(0) << ", " << w1_pos.at(1) << ")" // 2D debug...
	    << std::endl;
#endif

	  annihilate (w1_index, w1_pos);
	}

      else
	{
	  // Find a neighbor node, according to the kernel
	  const pos_type new_pos = kernel.random_node (w1_pos);
	  // Find the walker (if any) on that node
	  // asking its id (from 0 to total_walkers - 1)
	  const size_t w2_id = wt.id_of_who_is_in (new_pos);
	  // If you get total_walkers it means nobody is there,
	  // so move the walker
	  if (w2_id == total_walkers)
	    {
#ifdef DEBUG
	      // print some info about walkers in WT
	      std::cout
		<< "Walker " <<  wt.get_id (w1_index)
		<< " (index = " << w1_index << "): in"
		<< " (" << w1_pos.at(0) << ", " << w1_pos.at(1) << ") is going to" // 2D debug...
		<< " (" << new_pos.at(0) << ", " << new_pos.at(1) << ")."
		<< std::endl;
#endif
	      wt.set_pos (w1_index, new_pos);
	    }
	  // If someone is there, coalesce both of them
	  else
	    {
#ifdef DEBUG
	      // print some info about walkers in WT
	      std::cout
		<< "Walker " <<  wt.get_id (w1_index)
		<< " (index = " << w1_index << ") which was in"
		<< " (" << w1_pos.at(0) << ", " << w1_pos.at(1) << ") is coalescing into" // 2D debug...
		<< " Walker " <<  w2_id 
		<< " which is in"
		<< " (" << new_pos.at(0) << ", " << new_pos.at(1) << ")." // 2D debug...
		<< std::endl;
#endif
	      // w1_pos is the position "before" coalescence,
	      // no the position where coalescence happens
	      coalesce (w1_index, w2_id, w1_pos);
	    }
	}
    };
}


// returns an unfilled cladogram
template <typename KERNEL_TYPE, unsigned short int dim>
CCladogram
CCoalescent<KERNEL_TYPE,dim>::alloc_cladogram ()
{
  CCladogram clad (wt.lattice_side ());
  return clad;
}

template <typename KERNEL_TYPE, unsigned short int dim>
void
CCoalescent<KERNEL_TYPE,dim>::get_cladogram (CCladogram & clad)
{
  // run a coalescent process filling a wt
  run ();
  // Overwrite @clad with the info of recent wt
  clad.set (wt);
}


#endif
