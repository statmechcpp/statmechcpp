#ifndef CWALKER_H
#define CWALKER_H 

#include <cstdlib>
#include <boost/array.hpp>

template <unsigned short int dim>
class CWalker
{
  // State: should be accessed through the respective functions,
  // We are not blocking direct access to position to let some flexibility
private:

  // unique number which always identifies the same walker
  // (values go from 0 to side^dim-1)
  size_t id;

  // id of the walker into which this walker coalesced.
  //
  // the initial value is side^dim, but will be overwritten if the walker
  // just coalesce into another. After the simulation, those walkers which
  // still retain value side^dim will evidence that they were annihilated, not
  // coalesced into other walker, although probably a lot of walkers coalesced
  // into it.
  //
  // That is, this field is only set for one of the two walkers which coalesce,
  // so we are understanding coalescence as an asymetrical process.
  // This broken symetry is stated for:
  //    * (Less important) Knowing which walker had the role of parent and
  //      not of child when coalescing, and viceverse.
  //    * (More important) Keeping saved the info needed to solve the species
  //      associated to every walker, no less, no more.
  size_t coalesced_into;
  // double birth_date; // when the walker was generated in the past

public:
  typedef boost::array<long int,dim> pos_type;
  pos_type current_pos; // current position of the walker

  // Default ctor (called when creating a container of it)
  //
  //  VERY IMPORTANT: 
  //
  //  * We don't know number of walkers for setting flags
  //    Set id and coalesced_into by yourself
  //
  // CWalker () : id(0), coalesced_with(0) {}

  // Reading functions
  size_t get_id () const {return id;}
  size_t get_coalesced_into () const {return coalesced_into;}
  pos_type get_pos () const {return current_pos;}

  // Writting functions
  void set_id (size_t val) {id = val;}
  void set_coalesced_into (size_t val) {coalesced_into = val;}
  void set_pos (const pos_type & node) {current_pos = node;}
};

#endif
