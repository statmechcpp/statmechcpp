#include <cstdlib>
#include <iostream>
#include "../kernels/CKernelNN_2D_8_inf.hpp"
#include "CCoalescent.hpp"
//#include "CWalkerTable.hpp"
//#include "CCladogram.hpp"
//#include "CRSA.hpp"

int
main ()
{
  const double nu = 0.001;
  const size_t side = 50;
  const size_t iterations = 1000;

  CCoalescent<CKernelNN_2D_8_inf<long int>, 2> coalescent(nu, side);
  CCladogram clad(coalescent.alloc_cladogram ());
  CRSA rsa(clad.alloc_RSA ());

  for (size_t i = 1; i <= iterations; i++)
    {
      coalescent.get_cladogram (clad);
      clad.measure_RSA (rsa);
      rsa.print_pdf ("rsa.sal");
    }

  return 0;
}
