#ifndef CCLADOGRAM_H
#define CCLADOGRAM_H

/*
 * This class expect a walker table such that:
 *  . Walkers coalesced into the walker with id pointed in @coalesced_with, but
 *  . if @coalesced_into == @side^dim, then the walker died by annihilation
 */

#include <cstdlib>
#include <stdexcept>
#include <vector>

#include "../gsl/CLogHisto.hpp"
#include "CWalkerTable.hpp"
#include "CRSA.hpp"

class CCladogram
{
  //class CCladEntry;
  class CCladEntry
  {
  public:
    // a unique ID which identifies all the individuals of the same specie
    size_t species_id;

    // index of the individual M (for main) which shares with the individual
    // of this entry D (for derived) a closer, in time, ancestor A than the
    // others. This condition is symetric for both of them, BUT we don't
    // want both individuals links to themselves, so:
    //
    // The adittional condition is that the walker associated to D coalesced
    // into the walker associated to M.
    //
    // In terms of ancestors, it means:
    //   * M == A: closer ancestor in common of D is the alive individual M
    //   * M is a descendent of a child of A which was born AFTER other child
    //      of A which continued the species until D.
    //
    // I say AFTER because, backward in time, the first walker which meets A
    // will be silently the walker (of interest) which represents A. Then,
    // sometime BEFORE, (without motion of walker A) the walker associated to
    // D is derived.
    //
    // I don't promise that last deduction is true, but I convinced myself when
    // I wrote it. At any case, it is not needed for correction of the code.

    size_t main_relative_index; // id of individual is the index + 1 (I need 0...)
  };

  typedef std::vector<CCladEntry> cladogram_type;
  
  const size_t lattice_side_val;
  cladogram_type clad;
  size_t n_species;

  // helper function of set ()
  // it modifies the contents of clad
  size_t find_and_set_species_id (size_t index);

public:
  CCladogram (size_t side) : lattice_side_val(side), clad(side*side), n_species(0) {}
  size_t size () const {return clad.size ();}
  size_t lattice_side () const {return lattice_side_val;}

  // ctor-like: set the cladogram with the info provided by the walker table
  //            but it uses always the same memory, allocated by real ctor
  template <unsigned short int dim>
  void set (const CWalkerTable<dim> &);

  // number of different species
  size_t get_n_species () const {return n_species;}
  CRSA alloc_RSA (size_t side) const;
  CRSA alloc_RSA () const {return alloc_RSA (lattice_side ());}
  void measure_RSA (CRSA &) const;
  //  void measure_SAR (CLogHisto &) const;
};

/*
 * set (wt)
 *
 * Description:
 *
 *  Given a WT, it creates a cladogram. Each walker of the WT will be
 *  associated with an individual on the original position of the walker,
 *  (assignated by the function CWalkerTable<dim>::iterate_position),
 *  and the species of these inidividuals are identified by an
 *  arbitrary value between 0 and the number of different species - 1.
 *
 *  You can deduce the species knowing that, given an individual, it
 *  belongs to the same species than the individual associated to the
 * walker which id is in the field @coalesced_into.
 *
 * @wt must be a correct WT. That is, if the WT has N entries:
 *  * The @id of any entry must have an unique value between 0 and N-1
 *  * The @coalesced_with must be N or the id of any other walker,
 *    respecting the rules of the genealogical tree represented above.
 *    E.g.: I cannot coalesce into the walker which coalesced into me.
 *  * Every walker with @coalesced_with == N will be interpreted as
 *    the lonely walker which reached the first individual of an
 *    unique species.
 */

template <unsigned short int dim>
void
CCladogram::set (const CWalkerTable<dim> & wt)
{
  // check consistency
  if (clad.size () != wt.total_walkers ())
    {
      throw std::invalid_argument("WalkerTable has different dimensions than expected!");
    };

  // counter of different species found
  size_t k = 0;

  // copy all the info you need from WalkerTable, but SORTING so
  // id's are placed in natural order and you may recover original positions
  for (size_t index_in_wt = 0; index_in_wt < clad.size (); index_in_wt++)
    {
      const size_t index_in_clad = wt.get_id (index_in_wt);
      const size_t index_rel_in_clad = wt.get_coalesced_into (index_in_wt);

      // cp coalesced_into but with a different name
      clad[index_in_clad].main_relative_index = index_rel_in_clad;

      if (index_rel_in_clad == clad.size ())
	// Assign a new specie to the walker associated to every first ancestor
	clad[index_in_clad].species_id = k++;
      else
	// Your specie will be calculated later (size () used as a flag)
	clad[index_in_clad].species_id = clad.size ();
    }
  // Now you know how many different species there are (indexed from 0 to k-1)
  n_species = k;

  // Finally, set the species_id associated to every walker which not reached 
  // the first ancestor of a species
  for (size_t clad_index = 0; clad_index < clad.size (); clad_index++)
    find_and_set_species_id (clad_index);
}


#endif
