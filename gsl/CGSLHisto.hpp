#ifndef GSL_CGSLHISTO_HPP
#define GSL_CGSLHISTO_HPP

/*
 * CGSLHisto<bool count_calls = false, bool count_sums = false>
 * 
 * Brief:
 *
 * CGSLHisto is useful when you are more interested in how values change
 *  across bins, not in understanding statistics of single bins.
 *
 * This template is a wrapper for GSL Histograms in a OOP style, and a bit
 * more. If you know gsl_histogram commands, you should feel comfortable using
 * it.
 *
 * The behavior has being intended to respect the GNU (and GSL) style:
 *
 *   * Checking bounds.
 *   * Returning appropriated GSL states values.
 *   * Invoking GSL error handlers as the GSL would do it.
 *
 * Some code has been copied from the GSL Histogram's source code to be adapted
 * to the aditional features.
 *
 * Additional features:
 *
 * * It can remember the number of times that you incremented a bin.
 *
 *   Of course, if you always use the increment function, that info is just the
 *    value on that bin. However, if you use accumulate, you can use this
 *    information for knowing the mean weight passed to each bin.
 *
 *   It is very useful when you are interested in knowing the usual value
 *    that a bin accumulates, SUPPOSING COORDINATES IN THE RANGE OF THE BIN
 *    ACCUMULATE IDENTICALLY.
 *
 *   Usually you will NOT be interested in dividing by width (!) since
 *    that answers to the question "Which bin did count more according to
 *    its width?". On the other hand, after dividing the total value n_i
 *    accumulated in some bin by the number of accumulations c_i on that bin,
 *    you get the mean accumulation a_i = n_i / c_i in the bin. If two bins
 *    (with probably different widths w_i) output the same a_i, you can assert
 *    that the representative coordinate of those bins usually accumulate the
 *    same in mean, independently of w_i..
 *
 * Note: ---------
 *
 * However, c_i / w_i is proportional to the probability of a bin to be called
 * if supossing that the number of calls a bin can recieve is proportional
 * to the width of the bin. That is an interesting measure, for example
 * to calculate the probability of reaching some bin. If you assure that
 * the max number of calls that a bin can recieve is N*w_i/k, then the
 * representative probability, in the sense that prob=1 occurs when the
 * bin is called w_i/k times (so recieving just one calls gives prob != 1) is
 * 
 *  P_i = c_i * k / (N * w_i) 
 *
 * End note -------
 *
 *   You can get the mean sum A of total mean accumulations by integration
 *
 *       A = int_min^max a(x) ~ sum_i { a_i * w_i }
 *
 *   If you want/can understand it as a pdf, you can just normalize
 *
 *       p(x) = a/A 
 *
 *   Note:
 *
 *   In the case that you pass data FROM A SINGLE COORDINATE to each bin:
 *
 *     1. You want to specify the ranges using the single triggering 
 *         coordinate as the left value of the respective bin. When you have,
 *         to use a representative coordinate for the bin, use just that value.
 *
 *         Set the right value of the last range to whatever
 *         bigger than last coordinate.
 *
 *     2. If you can anticipate the number of times that each bin
 *         will be triggered, you don't need this feature (!).
 *         Specially, if it is identical for each bin, you can
 *         just divide the result of every bin by that number.
 *
 *
 * * Dividing values of any bin by width you can see the histogram as a pdf.
 *
 *   Dividing by width is useful when considering that the coordinate to
 *    increment is chosen according to a probability distribution function
 *    (pdf).
 *
 *   If the width w_i of a bin is twice than other, the first should have a
 *    number of counts n_i which be twice bigger for asserting than the p(x)
 *    has a similar value in the coordinates of both ranges. This way, the 
 *    probability associated to the representative coordinate x_i satisfies:
 *
 *       p(x_i) ~ n_i/w_i.
 *
 *    You'll get a normalized pdf (satisfying 1 = int_min^max p(x) ~
 *     sum_i { p(x_i) * w_i }) when p(x_i) = n_i/ (N w_i), being
 *     N = sum_i {n_i}.
 *
 *    See: https://www.gnu.org/software/gsl/manual/html_node/
 *           Resampling-from-histograms.html#Resampling-from-histograms
 *
 *   The basis for understanding the histo as a pdf is assuming a
 *    UNIFORM DISTRIBUTION OF EVENTS IN EACH BIN, so:
 *
 *      1. The bin should be enough narrow for mapping a range with a similar
 *         p(x_i) ~ p(x_i_left) ~ p(x_i_right)
 *
 *      2. The coordinates which "really" trigger (due to computational
 *         limitations) are distributed homogeneously in the bin, which usually
 *         is satisfied when having an enough number of triggering coordinates
 *         inside.
 *
 *   You could yet get useful data in situations of sparse events if you know
 *    the spatiation of the events and you specify the bin ranges in such a
 *    way that:
 *
 *      * The number of triggering coordinates on bins are proportional to
 *        the bin width. Then dividing by width you are dividing by a density.
 *
 * * It can save the sum of all coordinates used to reference a bin.
 *
 *   That is useful if you want to know the better coordinate which represents
 *    a bin, e.g., when plotting the histo or when calculating statistics such
 *    as the mean or the variance.
 *
 *   However, the better representative coordinate of a bin, when bins are
 *    narrow, is not very different to the middle value of the bin range.
 *
 *   Of course, it is only possible if you accumulate specifying a coordinate,
 *     no guessing by yourself the index bin (!)
 *
 * * You can specify a "virtual" scale for the ranges.
 *
 *   Sometimes, the values you want to measure are not in the scale you want
 *    to use when representing the information. E.g., you could have values
 *    in seconds but you want to finish with minutes. An option would be to
 *    set a range in the desired scale but multiply every value by the scaling
 *    factor before passing it to the histogram.
 *
 *   However, it is a pity to do so much conversions at the beginning if you
 *    can wait until the histogram outputs and then scale only a few values.
 *
 *   This feature allows you to specify such a scale so the histogram will
 *    work internally with the original data, but when printing information,
 *    it will return it as if data was passed scaled all the time. It apply
 *    when:
 *
 *   * Printing ranges, or the representative coordinate of a range.
 *   * Printing statistics, such as the sum, mean or sigma.
 *
 */

#include <gsl/gsl_histogram.h>
#include <gsl/gsl_errno.h>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include <vector>
#include <cassert>
#include <cmath>

template<bool s_calls = true, bool s_totals = true>
class CGSLHisto
{
  gsl_histogram * h; // the histogram
  bool is_h_allocated; // allocation state of h

  /*
   * As user of this class, you are forzed to define the range during
   * construction.
   * However, a derived class can take the responsability of defining it later
   * (use the constructor without arguments).
   *
   */
  std::vector<size_t> calls; // number of times you added values to some bin
  std::vector<double> totals; // the sum of the coordinates used to reference
                              // an incremented bin ('accumulate' sums by
                              // weight times)

  double scale_factor; // simulate scaled ranges when returning information
  double accum_factor; // simulate every increment was weighted by this

  enum ViewMode {histo /*default*/,
		 pdf,
		 calls_smoother, /* NEEDS template parameter s_calls */
		 calling_prob    /* NEEDS template parameter s_calls */
		 };
  ViewMode view_mode;

  enum ReprCoord {middle /*default*/,
		  exact /* NEEDS template parameter s_totals */,
		  left};

  ReprCoord repr_coord;

  void alloc (size_t n_bins); // helper with common allocation instructions
  void reset_calls ();
  void reset_totals ();

protected:
  CGSLHisto (); // DANGER, DANGER! call set_ranges before calling another f!

public:

  // -------- Creating a new object of this class -----------

  // When the user call SET_RANGES or SET_RANGES_UNIFORM, last histogram is
  // freed and a new one is allocated and initializated to 0.
  // That is slightly different than gsl behaviour, which must respect
  // the number of bins set when allocated

  // general form: provide ranges to be used for the bins
  int set_ranges (const double range[], size_t range_size);
  CGSLHisto (const double range[], size_t range_size);

  // uniform: histogram with bins of equal width.
  int set_ranges_uniform (double xmin, double xmax, size_t n_bins);
  CGSLHisto (double xmin, double xmax, size_t n_bins = 200);

  ~CGSLHisto ();
  CGSLHisto (const CGSLHisto &);
  CGSLHisto & operator= (const CGSLHisto &);

  // ----- commands for incrementing counts ------

  int increment (double x); // increment by 1 the bin which x is inside range
  int increment_by_index (size_t i); // idem but choosing bin by hand (unusual)
  int accumulate (double x, double weight); // increment by weight
  int accumulate_by_index (size_t i, double weight);
  int set (double x, double new_value); // just set a bin to that value
  int set_by_index (size_t i, double new_value);
  
  void reset ();

  // ---- output modifiers ---- 
  //
  // NOTE: the functions on this block do not change bins' data,
  // but values returned by member functions 
  //
  // You can set them all the times you want

  // Set factor to use when returning range information 
  void set_scale_factor (double f) {scale_factor = f;} // default value is 1.
  // Set factor to use when returning counting information
  // (unuseful if asked with normalization)  
  void set_accum_factor (double f) {accum_factor = f;} // default value is 1.

  // NOTE:
  // member functions named input_* return values not modified by scale_factor.
  // if scale_factor was not set (or set to 1), both versions are equivalent

  // specify if using the exact representative coordinate of a range
  // or the mean value
  // NEEDS parameter s_totals == true
  void set_middle_repr () {repr_coord = middle;}
  void set_exact_repr () {assert(s_totals); repr_coord = exact;}
  void set_left_repr () {repr_coord = left;}

  // specify how to understand the bin
  // member functions will return and output according to it 
  void view_as_histo () {view_mode = histo;}
  void view_as_pdf () {view_mode = pdf;}
  void view_as_calls_smoother () {assert(s_calls); view_mode = calls_smoother;}
  void view_as_calling_prob () {assert(s_calls); view_mode = calling_prob;}

  // ----- get info from ranges -------

  // get coordinates limiting a bin
  int input_range (size_t i, double * lower, double * upper) const
  {return gsl_histogram_get_range (h, i, lower, upper);}
  int range (size_t i, double * lower, double * upper) const;
  // number of bins
  size_t bins () const {return gsl_histogram_bins (h);}
  // biggest coordinate of the range
  double input_max () const {return gsl_histogram_max (h);}
  // lower coordinate of the range
  double input_min () const {return gsl_histogram_min (h);}

  double max () const {return scale_factor * input_max ();}
  double min () const {return scale_factor * input_min ();}

  // representative coordinate of a bin. By default: (lower (i) + upper(i))/2
  double input_repr (size_t i) const;
  double repr (size_t i) const {return scale_factor * input_repr (i);}

  // write to i the bin which contains coordinate x 
  int input_find (double x, size_t * i) const
  {return gsl_histogram_find (h, x, i);}                                       
  int find (double x, size_t * i) const
  {return gsl_histogram_find (h, x/scale_factor, i);} // yes, division



  // ------ read values on bins --------

  // the value of the bin i as stored internally (it checks bounds)
  double raw_content (size_t i) const {return gsl_histogram_get (h, i);}
  // the value of the bin i the way the user asked
  double get (size_t i, bool normalize = false) const;

  // the contribution of the ENTIRE bin used when summing (~integrating)
  //   if histo, obviously: ni
  //   if pdf:              ni    (~ wi*pi, since pi = ni / (N wi) )
  //   if calls_smoother:   ni/ci
  double bin_contrib (size_t i) const;

  // number of times values were added to bin i
  size_t get_calls (size_t i) const {assert (s_calls); return calls.at(i);}

  // sum of all coordinates used as a reference to increment bin i
  // WARNING: don't confuse with sum ()
  double get_total (size_t i) const {assert (s_totals); return totals.at(i);}

  // ----- statistics: ------- 

  // sum all the bin contributions. what would you expect from sum?
  //
  //  histo: the number of increments N = sum_i {n_i}
  //  pdf: the area under the curve: N = sum_i {n_i/w_i * w_i} = sum_i {n_i}
  //  call_smoother: the total mean accumulation: A = sim_i {n_i/c_i} 
  //
  // WARNING: don't confuse with get_total (size_t)
  double sum (bool norma_check = false) const;
  // Calculate the mean accoring to chosen coordinate representant and
  // bin_contrib ()
  double mean () const;
  // Idem for sigma
  double sigma () const; // sigma == sqrt(variance)
  // Alternative form useful if you are interested in both
  void stats (double * mean, double * sigma) const;

  // -----   printing ----------

  // print to standard output
  void print (bool normalize = false,
	      bool skip_zeros = false,
	      bool use_repr = false,
	      bool first_line_stats = true,
	      bool online_stats = false) const;
  // print to file (overwriting)
  void print (const char * filename,
	      bool normalize = false,
	      bool skip_zeros = false,
	      bool use_repr = false,
	      bool first_line_stats = true,
	      bool online_stats = false) const;
  // print to output stream
  void print (std::ostream & out,
	      bool normalize = false,
	      bool skip_zeros = false,
	      bool use_repr = false,
	      bool first_line_stats = true,
	      bool online_stats = false) const;
};

/*********** Definitions *********/

/* Note for the mantainer:
 *
 *  * Use the GSL functions when some cheking must be done
 *  * Access the histogram structure directly only if it is
 *     clean and totally safe
 */


/* alloc:
 * Common code used when allocating.
 * If this function was called before, previous historgram is freed.
 */  
template<bool s_calls, bool s_totals>
void
CGSLHisto<s_calls,s_totals>::alloc (size_t n_bins)
{
  // consider it is not the first time this function is called
  if (is_h_allocated)
    {
      is_h_allocated = false;
      gsl_histogram_free (h);
    }  
  // allocate the histo
  h = gsl_histogram_alloc (n_bins);
  is_h_allocated = true;

  // resize and set the containers to 0
  if (s_totals) totals.assign (n_bins, 0.0);
  if (s_calls) calls.assign (n_bins, 0);
}

/* set_ranges:
 *
 * After calling this function,
 * an histogram is allocated and the ranges are defined.
 * After a successful call to this function, the user can call the rest of
 * functions of this object, like increment.
 *
 * it uses alloc (see notes there)
 */
template<bool s_calls, bool s_totals>
int
CGSLHisto<s_calls,s_totals>::set_ranges (const double range[], size_t range_size)
{
  alloc (range_size - 1);
  // set ranges and fill bins to 0 (done by the gsl)
  return gsl_histogram_set_ranges (h, range, range_size); 
}

/* set_ranges_uniform:
 *
 * After calling this function,
 * an histogram is allocated and the ranges are defined.
 * After a successful call to this function, the user can call the rest of
 * functions of this object, like increment.
 *
 * it uses alloc (see notes there)
 */
template<bool s_calls, bool s_totals>
int
CGSLHisto<s_calls,s_totals>::set_ranges_uniform (double xmin, double xmax, size_t n_bins)
{
  alloc (n_bins);
  // set ranges and fill bins to 0 (done by the gsl)
  return gsl_histogram_set_ranges_uniform (h, xmin, xmax);
}

// non-allocating Ctor (!)
template<bool s_calls, bool s_totals>
CGSLHisto<s_calls,s_totals>::CGSLHisto ()
  : is_h_allocated(false),
    scale_factor(1.),
    accum_factor(1.),
    view_mode(histo),
    repr_coord(middle)
{}
/*
  Ctor with uniform range facility
 */
template<bool s_calls, bool s_totals>
CGSLHisto<s_calls,s_totals>::CGSLHisto (double min, double max, size_t n_bins)
  : is_h_allocated(false),
    scale_factor(1.),
    accum_factor(1.),
    view_mode(histo),
    repr_coord(middle)
{
  // allocation is done in set_ranges_uniform
  set_ranges_uniform (min, max, n_bins);
}

/*
 * generic constructor
 */
template<bool s_calls, bool s_totals>
CGSLHisto<s_calls,s_totals>::CGSLHisto (const double range[], size_t range_size)
  : is_h_allocated(false),
    scale_factor(1.),
    accum_factor(1.),
    view_mode(histo),
    repr_coord(middle)
{
  // allocation is done in set_ranges
  set_ranges (range, range_size);
}

// Copy-Ctor
template<bool s_calls, bool s_totals>
CGSLHisto<s_calls,s_totals>::CGSLHisto (const CGSLHisto & other)
  : h(gsl_histogram_clone (other.h)),
    is_h_allocated(true),
    calls(other.calls),
    totals(other.totals),
    scale_factor(other.scale_factor),
    accum_factor(other.accum_factor),
    view_mode(other.view_mode),
    repr_coord(other.repr_coord)
{}

// Assignment operator
// it accepts histograms with different numbers of bins
template<bool s_calls, bool s_totals>
CGSLHisto<s_calls,s_totals> &
CGSLHisto<s_calls,s_totals>::operator= (const CGSLHisto &other)
{
  // if size is equal, memcopy
  if (bins () == other.bins ())
    gsl_histogram_memcpy (h, other.h);
  // ...else, free the current one and clone
  else
    {
      gsl_histogram_free (h);
      h = gsl_histogram_clone (other.h);
    }
  is_h_allocated = true;
  calls = other.calls;
  totals = other.totals;
  scale_factor = other.scale_factor;
  accum_factor = other.accum_factor;
  view_mode = other.view_mode;
  repr_coord = other.repr_coord;

  return *this;
}

// Dtor
template<bool s_calls, bool s_totals>
CGSLHisto<s_calls,s_totals>::~CGSLHisto ()
{
  if (is_h_allocated) gsl_histogram_free (h);
  is_h_allocated = false;
}


template<bool s_calls, bool s_totals>
void
CGSLHisto<s_calls,s_totals>::reset_calls ()
{
  assert (s_calls);
  // calls is sized when ranges are set
  calls.assign (calls.size (), 0);
}

template<bool s_calls, bool s_totals>
void
CGSLHisto<s_calls,s_totals>::reset_totals ()
{
  assert (s_totals);
  // totals is sized when ranges are set
  totals.assign (totals.size (), 0.0);
}


template<bool s_calls, bool s_totals>
void
CGSLHisto<s_calls,s_totals>::reset ()
{
  gsl_histogram_reset (h);
  if (s_calls) reset_calls ();
  if (s_totals) reset_totals ();
}

// ----------- Ordinary functions ---------------------

template<bool s_calls, bool s_totals>
int
CGSLHisto<s_calls,s_totals>::increment_by_index (size_t i)
{
  return accumulate_by_index (i, 1.0);
}

// It is intended to be similar to gsl_histogram_find:
// If index is wrong, invoke the error handler
template<bool s_calls, bool s_totals>
int
CGSLHisto<s_calls,s_totals>::accumulate_by_index (size_t i, double weight)
{
  // No sense calculate sums if you don't specify a value for the sum
  assert (!s_totals);
  // Check bounds limits
  if (i < h->n)
    {
      h->bin[i] += weight;    
      if (s_calls) calls[i]++;
    }
  // Honor the GSL 
  else
    {    
      GSL_ERROR_VAL ("index lies outside valid range of 0 .. n - 1",
		     GSL_EDOM, 0);
    }
  return 0;
}

// It is intended to be similar to gsl_histogram_find:
// If index is wrong, invoke the error handler
template<bool s_calls, bool s_totals>
int
CGSLHisto<s_calls,s_totals>::set_by_index (size_t i, double new_value)
{
  // No sense calculate sums if you don't specify a weight
  assert (!s_totals);
  // Check bounds limits
  if (i < h->n)
    {
      h->bin[i] = new_value;    
      if (s_calls) calls[i]++;
    }
  // Honor the GSL 
  else
    {    
      GSL_ERROR_VAL ("index lies outside valid range of 0 .. n - 1",
		     GSL_EDOM, 0);
    }
  return 0;
}


template<bool s_calls, bool s_totals>
int
CGSLHisto<s_calls,s_totals>::increment (double x)
{
  return accumulate (x, 1.0);
}

// increase the bin which match coordinate x a quantity weight
// if x is outside of ranges, do NOTHING
template<bool s_calls, bool s_totals>
int
CGSLHisto<s_calls,s_totals>::accumulate (double x, double weight)
{
  /* code adapted from the original function, in: histogram/add.c
   * we did it the hard way for uniforming the code for s_totals ans s_calls.
   * Now, if the code has an error, all the cases are wrong, not only one;
   * That bug is easier to detect.
   *
   * After enough testing, a template specialization for <false,false>
   * with just gsl_histogram_accumulate would be nice.
   */

  // Check limits and, if x is out of range, simply discard the data 
  // and avoid error handling by gsl_histogram_find
  if ( x < gsl_histogram_min (h) || x >= gsl_histogram_max (h) )
    return GSL_EDOM;

  // find index relative to x
  size_t i;
  // to hard-code here GSL-internal 'find' function would be less redundant,
  // but it is complex and... who cares an additional pair of if's
  gsl_histogram_find (h, x, &i);

  // update histo
  h->bin[i] += weight;
  if (s_totals) totals[i] += x*weight;
  if (s_calls) calls[i]++;

  return GSL_SUCCESS;
}

template<bool s_calls, bool s_totals>
int
CGSLHisto<s_calls,s_totals>::set (double x, double new_value)
{
  assert(!s_totals); // No sense calculate sums if you don't specify a weight

  // Check limits and, if x is out of range, simply discard the data 
  // and avoid error handling by gsl_histogram_find
  if ( x < gsl_histogram_min (h) || x >= gsl_histogram_max (h) )
    return GSL_EDOM;

  // find index relative to x
  size_t i;
  // to hard-code here GSL-internal 'find' function would be less redundant,
  // but it is complex and... who cares an additional pair of if's
  gsl_histogram_find (h, x, &i);

  // update histo
  h->bin[i] = new_value;
  if (s_calls) calls[i]++;

  return GSL_SUCCESS;
}


template<bool s_calls, bool s_totals>
double
CGSLHisto<s_calls,s_totals>::bin_contrib (size_t i) const
{
  // Check bounds
  if (i >= h->n)
    {
      GSL_ERROR_VAL ("index lies outside valid range of 0 .. n - 1",
		     GSL_EDOM, 0);
    }

  double contrib = -1.;

  switch (view_mode)
    {
    case calls_smoother:
      contrib = (0 == calls[i]) ? 0. : h->bin[i]/calls[i]; break;
    case calling_prob:
      contrib = calls[i] / (h->range[i+1] - h->range[i]) ; break;
    default:
      contrib = h->bin[i]; break;
    }

  return contrib;
}


// return content of the bin but altered by user-requeriments
template<bool s_calls, bool s_totals>
double
CGSLHisto<s_calls,s_totals>::get (size_t i, bool normalize) const
{
  // contribution of the bin (it checks bounds)
  double val = bin_contrib (i);
  // scale it
  val *= accum_factor; 
 
  // if pdf, it must return the density of prob. for coordinates inside range:
  // divide by the (scaled) width of the bin
  if (pdf == view_mode) val /= (scale_factor * (h->range[i+1] - h->range[i]));
  
  if (normalize)
    val /= sum ();

  return val;
}

// returns coordinate which represents a bin:
template<bool s_calls, bool s_totals>
double
CGSLHisto<s_calls,s_totals>::input_repr (size_t i) const
{
  // Check bounds
  if (i >= h->n)
    {
      GSL_ERROR_VAL ("index lies outside valid range of 0 .. n - 1",
		     GSL_EDOM, 0);
    }
  // choose representative coordinate according to exact_repr
  // (if repr_coord is exact, s_calls is assured to be true)
  double repr = -1.;
  switch ( repr_coord )
    {
    case exact:  repr = totals[i] / h->bin[i]; break;
    case middle: repr = (h->range[i+1] + h->range[i]) / 2; break;
    case left:   repr =  h->range[i]; break;
    }
  return repr;
}

template<bool s_calls, bool s_totals>
int
CGSLHisto<s_calls,s_totals>::range (size_t i, double * lower, double * upper) const
{// code adapted from the original function, in: histogram/get.c
  // Check bounds
  if (i >= h->n)
    GSL_ERROR ("index lies outside valid range of 0 .. n - 1", GSL_EDOM);

  *lower = scale_factor * h->range[i];
  *upper = scale_factor * h->range[i + 1];

  return GSL_SUCCESS;
}

// -------- statistics -------------

// sum: sum all the bin contributions. what would you expect from sum?
//
//      histo: the number of increments N = sum_i {n_i}
//      pdf: the area under the curve: N = sum_i {n_i/w_i * w_i} = sum_i {n_i}
//      call_smoother: the total mean accumulation: A = sim_i {n_i/c_i} 
template<bool s_calls, bool s_totals>
double
CGSLHisto<s_calls,s_totals>::sum (bool norma_check) const
{
  // code adapted from the original function, in: histogram/stat.c
  long double sum = 0.;
  for (size_t i = 0; i < h->n; i++) 
    sum += bin_contrib (i);

  // correct the sum with factor specified by user
  sum *= accum_factor;

  // compute a sum with normalized values to check if it is really 1
  // it is what the user can do with the information provided by print
  if (norma_check)
    {
      const long double norma = sum;
      sum = 0.;
      // result should be 1 if everything is working
      for (size_t i = 0; i < h->n; i++)
	sum += accum_factor * bin_contrib (i) / norma;
    }
    
  return sum;
}

template<bool s_calls, bool s_totals>
double
CGSLHisto<s_calls,s_totals>::mean () const
{
  // code adapted from the original function, in: histogram/stat.c
  long double wmean = 0;
  long double W = 0;
  // compute the mean
  for (size_t i = 0; i < h->n; i++)
    {
      // deal only with weights bigger than 0
      // (it also avoiding a division by 0)
      if (h->bin[i] > 0)
	{
	  // use real scale and change it at the end
	  const double xi = input_repr (i);
	  const double wi = bin_contrib (i);

	  W += wi;
	  wmean += (xi - wmean) * (wi / W);
	}
    }
  return scale_factor * wmean;
}

template<bool s_calls, bool s_totals>
double
CGSLHisto<s_calls,s_totals>::sigma () const
{
  long double wvariance = 0.;
  long double wmean = 0.;
  long double W = 0.;
  // mean
  for (size_t i = 0; i < h->n; i++)
    {
      if (h->bin[i] > 0)
	{
	  const double xi = input_repr (i);
	  const double wi = bin_contrib (i);
	  W += wi;
	  wmean += (xi - wmean) * (wi / W);
	}
    }
  // variance
  W = 0.0;
  for (size_t i = 0; i < h->n; i++)
    {
      if (h->bin[i] > 0)
	{
	  const double xi = input_repr (i);
	  const double wi = bin_contrib (i);
	  const long double delta = (xi - wmean);
	  W += wi ;
	  wvariance += (delta * delta - wvariance) * (wi / W);
	}
    }
  double sigma = sqrt (wvariance);
  return scale_factor * sigma;
}

template<bool s_calls, bool s_totals>
void
CGSLHisto<s_calls,s_totals>::stats (double * mean, double * sigma) const
{
  long double wvariance = 0.;
  long double wmean = 0.;
  long double W = 0.;
  // mean
  for (size_t i = 0; i < h->n; i++)
    {
      if (h->bin[i] > 0)
	{
	  const double xi = input_repr (i);
	  const double wi = bin_contrib (i);
	  W += wi;
	  wmean += (xi - wmean) * (wi / W);
	}
    }
  // save mean
  *mean = scale_factor * wmean;

  // variance
  W = 0.0;
  for (size_t i = 0; i < h->n; i++)
    {
      if (h->bin[i] > 0)
	{
	  const double xi = input_repr (i);
	  const double wi = bin_contrib (i);
	  const long double delta = (xi - wmean);
	  W += wi ;
	  wvariance += (delta * delta - wvariance) * (wi / W);
	}
    }
  // save sigma
  *sigma = scale_factor * sqrt (wvariance);
}



template<bool s_calls, bool s_totals>
void
CGSLHisto<s_calls,s_totals>::print (std::ostream & out,
				    bool normalize,
				    bool skip_zeros,
				    bool use_repr,
				    bool first_line_stats,
				    bool online_stats) const
{
  // ----- Calculate statistics you need ------
  const bool calc_sum = normalize || first_line_stats || online_stats;
  const bool calc_mean_sigma = first_line_stats || online_stats;
  double sum_val = -1., mean_val = -1., sigma_val = -1.;
  if (calc_sum)
    sum_val = sum ();

  if (calc_mean_sigma)
    stats (&mean_val, &sigma_val);

  // normalization value
  double norma_factor = 1.0;  
  if (normalize) norma_factor = 1.0 / sum_val;

  // -------- Prepare output -----------
  // Output with max precision
  out.precision (15);

  // ----- Intial commentary -----
  if (first_line_stats)
    {
      // scale
      out << "# scale = " << scale_factor;
      if (normalize)
	out << ", factor (computed) = " << norma_factor;
      else
	out << ", factor (user-defined) = " << accum_factor;

      // sum, with different tag depending on view
      if (histo          == view_mode) out << ", SUM = ";
      if (pdf            == view_mode) out << ", INTEGRAL = ";
      if (calls_smoother == view_mode) out << ", MEAN_SUM = ";
      if (calling_prob   == view_mode) out << ", PROB_SUM = ";
      if (!normalize)
	out << sum_val;
      else
	{
	  const bool debug = true;
	  if (!debug) out << 1.;
	  else        out << sum (true);
	}

      // stats
      out << ", MEAN = "  << mean_val
	  << ", SIGMA = " << sigma_val
	  << std::endl;
    }

  // --------- Ordinary lines -----------
  for (size_t i = 0; i < h->n; i++)
    { 
      // get value es the user wants except, maybe, normalization
      const double val = get (i);
      if ( (! skip_zeros) || val != 0.)
	{
	  if (use_repr)
	    {
	      // Print representative coordinate
	      out << repr(i) << " ";
	    }
	  else
	    {
	      // Print range
	      double lower =-1, upper=-1;
	      this->range (i, &lower, &upper);
	      out << lower << " " << upper << " ";
	    }
	  // Print value, maybe normalized
	  out << get (i) * norma_factor;
	  // Stats
	  if (online_stats)
	    {
	      out << " " << sum_val
		  << " " << mean_val
		  << " " << sigma_val;
	    }
	  out << std::endl;
	}; // end skip_zeros
    }; // end bins loop
}

template<bool s_calls, bool s_totals>
void
CGSLHisto<s_calls,s_totals>::print (const char * filename,
				    bool normalize,
				    bool skip_zeros,
				    bool use_repr,
				    bool first_line_stats,
				    bool online_stats) const
{
  std::ofstream ofs;
  ofs.open (filename);
  print (ofs, normalize,
	 skip_zeros,
	 use_repr,
	 first_line_stats,
	 online_stats);
}

template<bool s_calls, bool s_totals>
void
CGSLHisto<s_calls,s_totals>::print (bool normalize,
				    bool skip_zeros,
				    bool use_repr,
				    bool first_line_stats,
				    bool online_stats) const
{
  print (std::cout, normalize,
	 skip_zeros,
	 use_repr,
	 first_line_stats,
	 online_stats);
}

#endif
