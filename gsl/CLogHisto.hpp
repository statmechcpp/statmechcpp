#ifndef CLOGHISTO_HPP
#define CLOGHISTO_HPP

/*
 * CLogHisto<save_calls, save_totals>
 *
 * This template manages a histogram of bins whose widths increase
 * exponentially, maybe with some optional restrictions.
 *
 * The easier way of using it is without any restrictions
 *
 *   CLogHisto<> h(0.1, 1e8, 500);
 *
 * This way you get an histo @h of 500 bins, the first bin starting at
 * 0.1 and positioning the limits of the rest bins by multiplying
 * successively the the previous limit by an exact factor so last bin
 * finishes in 1e8. The factor d is calculated as:
 *
 *    d = (1e8/0.1)^(1/500)
 *
 * A representation of bins' limits is the following:
 *
 * 0.1       0.1*d    0.1*d^2                  1e8/d^2     1e8/d       1e8
 *  | -------- | -------- | ---    .......   -----| -------- | -------- |
 *     #1 [0]      #2 [1]                          #499 [498] #500 [499]
 *
 *
 * If by some reason you want to specify the factor d, e.g. 1.12, instead of
 * the number of bins, you can do it when constructing h.
 *
 *   CLogHisto<> h(0.1, 1e8, false, 1.12);
 *
 * The 'false' value before last argument remembers you that it is not expecting
 * a number of bins, as above, but a factor.
 *
 * This form can be useful if you want different histograms starting at the same
 * value and finishing in different ones, but all of them sharing the same
 * ranges in the common interval.
 *
 * On the other hand, if you want to specify that all bin' widths must be equal to
 * or an entire multiple of some quantity, e.g. 1.4, you can use this constructor:
 *
 *   CLogHisto<> h(false, 0.1, 1e8, 1.4);
 *
 * The 'false' as first argument remembers you that this is a completely different
 * message.
 *
 * You can specify a 5th argument to indicate a factor as above, which will
 * suggest where to place the next limit, but it will be modified a bit
 * to respect the main condition.
 *
 * This form can be useful when you know that your input will be spatiated that
 * quantity and you want to understand the histogram as a pdf. However, the
 * prerequisite for doing that is that events must have a uniform distribution
 * of events in each bin.
 *
 * https://www.gnu.org/software/gsl/manual/html_node/
 *         Resampling-from-histograms.html#Resampling-from-histograms
 *
 * If by artificial restriction your code is blocking the triggering of events
 * in some bins, you could be in problems. If the size of your bins increases,
 * exponentially in this template, the following could be noticeable for lowest
 * bins (the bigger the bin, more triggering coordinates will be placed inside 
 * and better uniformly distributed will occur the events):
 *
 *   * You get some empty bins because no input is possible in their ranges,
 *     but it is something artificial.
 *   * Casually some bins are placed when a coordinate is triggering,
 *     but the count is not representative. Even more, when you divide
 *     by width, different for each bin, you can observe non-sense behaviors.
 *
 *
 * A good solution can be to divide by the number of possible coordinates,
 * which be predicted if triggering coordinates are equally spatiated.
 * This soultion appears automatically when the width of the bin is
 * proportional to that spatiation.
 *
 * If there is no constant spatiation, you should take care of this problem
 * by other means.
 *
 * In particular, the input could be restricted to natural numbers. In this
 * case something as the following could be used:
 *
 *   CLogHisto<> h(true, 0.1, 1e8, 1.);
 *
 * Then, all the bins will have an integer width. If you want to force that
 * extremes of the bins must be also integers, just specify it.
 *
 * You can use 0 as lower limit when forcing power-constant widths.
 *
 * TOTHINK: the representative coordinate could be in this case better selected
 *
 *    repr(i) = offset + width(i)/basic_width 
 *
 *  But probably the representative coordinate idea is just totally unuseful
 *   in real cases.
 *
 */

#include "CGSLHisto.hpp"

template<bool s_calls = true, bool s_totals = true>
class CLogHisto : public CGSLHisto<s_calls, s_totals>
{
  CLogHisto () {}
public:
  /* Constructor:
   * @min must be equal or lower than any number you want to include
   * @max must be strictly bigger than any number you want to register
   *
   */
  CLogHisto (double min, double max, size_t n_bins = 50);

  // * Pass false as 3rd argument for constructing range using a factor
  // * Real max will be bigger and match factor reqeriments for last bin
  CLogHisto (double min, double max,
	     bool specify_n_bins, // must be false (!)
	     double factor); // with default, overload

  // * Pass true as 1st argument
  // * min can be 0
  // * rfactor is the factor which will be followed, but range limits will be
  //    forzed to be atomic_width multiples
  CLogHisto (bool mult_widths, // must be false (!)
	     double min, double max,
	     double atomic_width, double rfactor = 1.15);

  // Next functions are used by ctors, but maybe you find them useful
  // 1st ctor
  double * get_exact_range (double a, double b, size_t n_bins) const;
  // 2nd ctor
  size_t bins_enough_range (double a, double b, double factor) const;
  double * get_enough_range (double a, double b, double factor) const;
  // 3rd ctor
  size_t bins_mult_widths_range (double a, double b,
				 double awidth, double rfactor) const;

  double * get_mult_widths_range (double a, double b,
				  double awidth, double rfactor) const;
  // debug
  void print_debug_ranges (double * range, size_t n_bins) const
  {
    std::cout << "--- Range information of CLogHisto: "
	      << "bins=" << n_bins << std::endl; 
    for (size_t i = 0; i <= n_bins; i++) std::cout << range[i] << std::endl;
  } 

};

template<bool s_calls, bool s_totals>
double *
CLogHisto<s_calls, s_totals>::get_exact_range (double min, double max, size_t n_bins) const
{
  assert (max > min);
  assert (min > 0.);
  // it's easy to calculate the factor needed if you want n_bins from
  // min to max spatiated logarithmically
  const double factor = pow (max/min, 1./(double) n_bins);
  double * range = new double[n_bins + 1];

  range[0] = min;
  for (size_t i = 1; i <= n_bins; i++)
    range[i] = range[i-1]*factor;

  return range;
}

template<bool s_calls, bool s_totals>
CLogHisto<s_calls, s_totals>::CLogHisto (double min, double max, size_t n_bins)
  : CGSLHisto<s_calls, s_totals>()
{
  // return a range for n_bins (will have n_bins+1 elements)
  double * range = get_exact_range (min, max, n_bins);

#ifdef HISTO_DEBUG
  print_debug_ranges (range, n_bins);
#endif

  this->set_ranges (range, n_bins + 1);
  delete[] range; // histogram is required to take an own copy of range  
}

// TODO: Using a vector and push_back would be cleaner and more robust.
//       Now, you have to calculate the number of bins, then allocate
//       and finally save the ranges on the array.

template<bool s_calls, bool s_totals>
size_t
CLogHisto<s_calls, s_totals>::bins_enough_range (double min, double max, double factor) const
{
  // do not do anything before checking options are passed correctly
  assert (factor > 1.);
  assert (max > min);
  assert (min > 0.);
  
  double next = min;
  size_t n = 0;
  while (max > next)
    {
      next *= factor;
      n++;
    }
  
  return n;
}


template<bool s_calls, bool s_totals>
double *
CLogHisto<s_calls, s_totals>::get_enough_range (double min, double max, double factor) const
{
  // will contain the n+1 values which specify the bin intervals
  const size_t n_bins = bins_enough_range (min, max, factor);
  double * range = new double[n_bins + 1];
  
  range[0] = min;
  size_t i = 0;
  while (max > range[i])
    {
      range[i+1] = factor*range[i];
      i++;
    }

  assert (i == n_bins);
  
  return range;
}

template<bool s_calls, bool s_totals>
CLogHisto<s_calls, s_totals>::CLogHisto (double min, double max,
			      bool specify_n_bins, double factor)
  : CGSLHisto<s_calls, s_totals> ()
{
  // check that user knows what is he asking
  assert (false == specify_n_bins);
  const size_t n_bins = bins_enough_range (min, max, factor);
  double * range = get_enough_range (min, max, factor);

#ifdef HISTO_DEBUG

  print_debug_ranges (range, n_bins);
#endif

  this->set_ranges (range, n_bins + 1);
  delete[] range; // histogram is required to take an own copy of range
}

// return number of bins of a partition built:
// * every range must be of width equal to atomic_width or a multiple of it
// * spatiated quasi-logarithmically when width >> atomic_width
template<bool s_calls, bool s_totals>
size_t
CLogHisto<s_calls, s_totals>::bins_mult_widths_range (double min, double max,
					   double awidth, double rfactor) const
{
  assert (min >= 0.);
  assert (max > min);
  assert (rfactor > 1.);
  assert (awidth > 0.);

  size_t n = 0;
  double current_limit = min;
  // last bin is counted, but the right extreme will probably be greater than
  // max considering it also has the expected well spatiated size
  while (max > current_limit)
    {
      // get theorical next limit multiplicating by a factor lower limit
      const double next_limit = current_limit * rfactor;

      if (next_limit < current_limit + awidth)
	current_limit += awidth;
      else
	{
	  // it is granted that @times is greater or equal than 1
	  const double times = floor ((next_limit - current_limit)/awidth);
	  current_limit += times*awidth;
	}
      n++;
    };

  return n;
}

template<bool s_calls, bool s_totals>
double *
CLogHisto<s_calls, s_totals>::get_mult_widths_range (double min, double max,
					  double awidth, double rfactor) const
{
  const size_t n_bins =
    bins_mult_widths_range (min, max, awidth, rfactor);
  double * range = new double[n_bins + 1];

  // Repeat code of 'bins_mult_widths_range' :(
  size_t i = 0;
  range[i] = min;
  while (max > range[i])
    {
      // theorical next limit multiplicating by a factor lower limit
      const double next_limit = range[i] * rfactor;
      // here I set the superior limit of bin i
      if (next_limit < range[i] + awidth)
	range[i+1] = range[i] + awidth;
      else
	{
	  // it is granted that @times is greater or equal than 1
	  const double times = floor ((next_limit - range[i])/awidth);
	  range[i+1] = range[i] + times*awidth;
	}

      i++;
    };
  // check consistency between two pieces of code (should be exactly the same)
  assert(i == n_bins);

  // OPTIONAL: correct last value
  // uncomment next line if you want to respect the value provided by user
  // for last bin.
  // At any case, probably the last bin should not have useful information
  //range[i] = max; // (range has size i+1, so index i reference last element)

  return range;
}

template<bool s_calls, bool s_totals>
CLogHisto<s_calls, s_totals>::CLogHisto (bool mult_widths,
			      double min, double max,
			      double awidth, double rfactor)
  : CGSLHisto<s_calls, s_totals> ()
{
  assert (false == mult_widths);

  const size_t n_bins = bins_mult_widths_range (min, max, awidth, rfactor);
  double * range = get_mult_widths_range (min, max, awidth, rfactor);

#ifdef HISTO_DEBUG
  print_debug_ranges (range, n_bins);
#endif
  this->set_ranges (range, n_bins);
  delete[] range; // histogram is required to take an own copy of range
}


#endif
