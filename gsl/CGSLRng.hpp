#ifndef CGSLRNG_H
#define CGSLRNG_H

#include <gsl/gsl_rng.h>

class CGSLRng
{
  static bool is_init;
  static gsl_rng * r;
  /* old idea, but complex causistic:

  static size_t instances;

  but:
     what if you repitedly removing existing instances and creating new ones?
     what about how initializing the new instances?
     one intance alive during all the program, with a unique seed and not
     worrying about freeing memory until done automatically when program
     finishes, seems the best solution
  */
public:
  CGSLRng ();
  CGSLRng (gsl_rng_type * T); /* Default seed: gsl_rng_default_seed == 0*/
  CGSLRng (gsl_rng_type * T, unsigned long int seed);
  ~CGSLRng ();

  void set (unsigned long int) const;
  unsigned long int get () const;
  double uniform () const;
  double uniform_pos () const;
  unsigned long int uniform_int (unsigned long int) const;

  const char * name () const;
  unsigned long int max () const;
  unsigned long int min () const;

  unsigned long int get_seed () const;
};

#endif
