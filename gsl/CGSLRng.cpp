#include <cstdlib>
#include <ctime>
#include <gsl/gsl_rng.h>
#include "CGSLRng.hpp"

bool CGSLRng::is_init = false;
gsl_rng * CGSLRng::r = NULL;

/*
 * Info about GSL Rng:
 *
 * The generator is automatically initialized with the variable 
 * @gsl_rng_default_seed, by default it is 0.
 *
 * You can change the value of @gsl_rng_default_seed and @gsl_rng_default
 * (by default, gsl_rng_mt19937) setting the environment variables
 * GSL_RNG_SEED and GSL_RNG_TYPE.
 */

// Read everything from the environment, but use a time-based seed if no one is
// chosen or it is 0
CGSLRng::CGSLRng ()
{
  if (! is_init)
    {
      is_init = true;
      // charge CMD option (or default if not specified)
      r = gsl_rng_alloc (gsl_rng_env_setup ());
      // change initial seed if not specified
      if (! getenv ("GSL_RNG_SEED")) gsl_rng_set (r, time (NULL));
    }
}
  
CGSLRng::CGSLRng (gsl_rng_type * T)
{
  if (! is_init)
    {
      is_init = true;
      r = gsl_rng_alloc (T);
    }
}

CGSLRng::CGSLRng (gsl_rng_type * T, unsigned long int seed)
{
  if (! is_init)
    {
      is_init = true;
      r = gsl_rng_alloc (T);
      gsl_rng_set (r, seed);
    }
} 

// current safe idea: just one instance, never free memory
CGSLRng::~CGSLRng ()
{
  //  if (instances == 1)
  //  gsl_rng_free (r);
  //else
  //    instances--;
}
void
CGSLRng::set (unsigned long int seed) const
{
  gsl_rng_set (r, seed);
}

unsigned long int
CGSLRng::get () const
{
 return gsl_rng_get (r);
}

double
CGSLRng::uniform () const
{
  return gsl_rng_uniform (r);
}

double
CGSLRng::uniform_pos () const
{
  return gsl_rng_uniform_pos (r);
}

unsigned long int
CGSLRng::uniform_int (unsigned long int n) const
{
  return gsl_rng_uniform_int (r, n);
}

const char *
CGSLRng::name () const
{
  return gsl_rng_name (r);
}

unsigned long int
CGSLRng::max () const
{
  return gsl_rng_max (r);
}


unsigned long int
CGSLRng:: min () const
{
  return gsl_rng_min (r);
}


unsigned long int
CGSLRng::get_seed () const
{
  return gsl_rng_default_seed;
}
