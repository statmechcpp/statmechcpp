#ifndef CLATTICE2D_H
#define CLATTICE2D_H

/*
 * CLattice2D<typename VALUE_TYPE,
 *            bool do_plot = false>
 *
 * Description:
 *
 * It can be used for managing a 2D rectangular lattice. If the non-type
 * parameter @do_plot is true, the lattice will be plotted in a separate
 * gnuplot's window. By default, every lattice modification will be showed
 * automatically!
 *
 * Internally, it uses boost::multiarray and gnuplot_i libraries. 
 *
 * WARNINGS:
 *
 * 1. @VALUE_TYPE should be the type of simple number if plotting.
 *    See Colors below.
 *
 * 2. It is not possible to specify a range different than [0,lattice_side).
 *    It could be implemented if needed. In fact, boost::multiarray has
 *    facilities for this.
 *
 * Usage:
 *
 * --- Construction ----
 *
 * Instanciate the template using the constructor, for example,
 *
 *  CLattice2D<int, true> l(10, 20, 3);
 *
 * It creates a lattice of 10x20.
 *
 * ---- Colors ----
 *
 * The third argument of the constructor
 * indicates how many colors will be reserved for plotting. In our example,
 * it is 3, so values 0, 1, 2 will have associated different colors. -1 will
 * have also an aditional reseved color (white) for a private use of this
 * class, but you have not to remember that.
 *
 * This third constructor argument will be useless if the third template 
 * parameter is false, and it can be ommited in this case. Else, it is
 * mandatory for correct visualization. Aditionally, if plotting,
 * @VALUE_TYPE template parameter must be something coherent with the fact
 * that it must be understood as a number in a range by gnuplot.
 *
 * ----- Accessing the lattice -----
 *
 * You are only allowed to access the lattice's elements through functions,
 * and never by references. This way, every element modification will be caught
 * and will be displayed if required. You can access to the elements with an
 * object of type @node_type or specifying individually both coordinates.
 *
 * Member functions available are:
 *
 *   VALUE_TYPE get_value (const node_type &) const;
 *   VALUE_TYPE get_value (index, index) const;
 *   void set_value (const node_type &, value_type value);
 *   void set_value (index, index,      value_type value);
 *
 * ------- Facilities ------------
 *  Some facilites have been implemented:
 *
 *   * Fill randomly the lattice through uniform_fill (n)
 *   * Get the sides and area of the lattice;
 *   * Get a random node which fall inside the lattice
 *
 * --------- Plotting ------------
 *
 *   * You can switch on/off the automatic update of the plot. Refreshing a
 *     position is slow, so it can be interesting to leave it off while filling
 *     the lattice manually. It is done by setting true/false the @auto_refresh
 *     member:
 *
 *        my_lattice.set_auto_refresh (false);     
 *
 *     NOTE: Call uniform_lattice will leave the plotting state unmodified.
 *
 *     You can plot the state of the lattice at any moment (useful only while
 *     auto_refresh is off) by calling plot.
 *
 *       my_lattice.plot ();
 *
 *     TODO: Documentation for managing plot_diff == false.
 */

#include "boost/multi_array.hpp"
#include <cstdlib>
#include <vector>
#include <unistd.h>
#include "../gsl/CGSLRng.hpp"
#include "../plot/CPlotLattice.hpp"

template <typename VALUE_TYPE, bool do_plot = false>
class CLattice2D
{
public:
  typedef VALUE_TYPE value_type;
  typedef typename boost::multi_array<VALUE_TYPE, 2> lattice_type;
  typedef typename lattice_type::index index;
  typedef typename boost::array<index, 2> node_type; // nodes contain coordinates
  
  //typedef typename lattice_type::iterator iterator2;
  //typedef typename lattice_type::const_iterator const_iterator2;
  //typedef typename lattice_type::template subarray<1>::type::iterator iterator;
  //typedef typename lattice_type::template subarray<1>::type::const_iterator const_iterator;

private:
  CGSLRng rng;
  lattice_type lattice;
  CPlotLattice canvas;
  const index size1;
  const index size2;
  const size_t area_value;
  // sent every difference to gnuplot or lost current state?
  bool plot_diff; // yet not tested with a false value (!)

public:
  
  // constructing
  CLattice2D (index dim1, index dim2, size_t n_colors = 0);

  // accessing data (read-only)
  const VALUE_TYPE & get_value (const node_type & node) const
  {return lattice(node);}
  const VALUE_TYPE & get_value (index i1, index i2) const 
  {return lattice[i1][i2];}
  VALUE_TYPE random_value () const;

  // accessing data (write-only)
  void set_value (const node_type &, const VALUE_TYPE value);
  void set_value (index, index, const VALUE_TYPE value);
  void uniform_fill (size_t n);
  
  // utilities
  node_type random_node () const;
  void random_node (node_type *) const;
  size_t area () const {return area_value;}
  index dim1 () const {return size1;}
  index dim2 () const {return size2;}

  // plotting
  bool is_auto_refresh () const {return canvas.is_auto_refresh ();}
  void set_auto_refresh (bool new_state) {canvas.set_auto_refresh (new_state);}
  void lattice2plot (); // send all the lattice to gnuplot
  void plot () {if (! plot_diff) lattice2plot (); canvas.plot ();}
  // if set to true, entire lattice is communicated to gp and maybe plotted
  //                 depending on @auto_refresh
  // if set to false, it stops communications with gp
  bool is_plot_diff () const {return plot_diff;}
  void set_plot_diff (bool new_state);
};

// @n is the number of different colors (indices from 0 to n-1) you will use
// @n has no sense if !do_plot
template <typename VALUE_TYPE, bool do_plot>
CLattice2D<VALUE_TYPE, do_plot>::
CLattice2D (index s1, index s2, size_t n)
  : rng (),
    lattice (boost::extents[s1][s2]),
    canvas (do_plot),
    size1 (s1),
    size2 (s2),
    area_value ((size_t) s1 * (size_t) s2),
    plot_diff (true)
{
  if (do_plot)
    {
      canvas.set (s1, s2, n);
      // Show a blank plot: the user will know things are working
      // although plot may be lating due to something such as filling
      canvas.plot ();
    }
}

// between 0 and @n-1
template <typename VALUE_TYPE, bool do_plot>
void 
CLattice2D<VALUE_TYPE, do_plot>::
uniform_fill (size_t n)
{
  // plotting initial nodes one by one is very slow
  // fill lattice without modifing the state of plot  vars
  for (index i1 = 0; i1 != size1; i1++)
    for (index i2 = 0; i2 != size2; i2++)
      lattice [i1] [i2] = rng.uniform_int (n);

  // Finally, depending on plot variables, act on plot
  if (do_plot && plot_diff) lattice2plot ();
}

template <typename VALUE_TYPE, bool do_plot>
void
CLattice2D<VALUE_TYPE, do_plot>::
lattice2plot ()
{
  for (index i1 = 0; i1 != size1; i1++)
    for (index i2 = 0; i2 != size2; i2++)
      canvas.node2plot (i1, i2, lattice [i1] [i2]);
}


template <typename VALUE_TYPE, bool do_plot>
void
CLattice2D<VALUE_TYPE, do_plot>::
set_value (const node_type & node, const VALUE_TYPE new_value)
{
  lattice(node) = new_value;
  //  if (do_plot && plot_diff) canvas.node2plot (node[0], node[1], new_value);
  if (do_plot && plot_diff) canvas.node2plot (node, new_value);
}

template <typename VALUE_TYPE, bool do_plot>
void
CLattice2D<VALUE_TYPE, do_plot>::
set_value (index index1, index index2, const VALUE_TYPE new_value)
{
  lattice [index1] [index2] = new_value;
  if (do_plot && plot_diff)
    {
      canvas.node2plot (index1, index2, new_value);
    }
}

/* random_node:
 *
 * Help facility which returns you a random valid node.
 */
template <typename VALUE_TYPE, bool do_plot>
typename CLattice2D<VALUE_TYPE, do_plot>::node_type
CLattice2D<VALUE_TYPE, do_plot>::random_node () const
{
  node_type v;
  // Way 1: chose a random coordinate for each dimension
  // v[0] = rng.uniform_int (size1);
  // v[1] = rng.uniform_int (size2);

  // Way 2: get a random value and calculate the coordinates by hand (!)
  const size_t n = rng.uniform_int (area_value);
  v[0] = n % size1;
  v[1] = n / size1;

  // Way 3: access lattice.data () + n;
  // OK, that way you have a pointer, but
  // How to get the coordinates?

  // const size_t n = rng.uniform_int (area);

  return v;
}

// similar, but writing node in the one passed as pointer
template <typename VALUE_TYPE, bool do_plot>
void
CLattice2D<VALUE_TYPE, do_plot>::
random_node (node_type * node) const
{
  // Way 2: get a random value and calculate the coordinates by hand (!)
  const size_t n = rng.uniform_int (area_value);
  (*node)[0] = n % size1;
  (*node)[1] = n / size1;
}


template <typename VALUE_TYPE, bool do_plot>
VALUE_TYPE
CLattice2D<VALUE_TYPE, do_plot>::
random_value () const
{
  return *(lattice.data () + rng.uniform_int (area_value));
}


template <typename VALUE_TYPE, bool do_plot>
void
CLattice2D<VALUE_TYPE, do_plot>::
set_plot_diff (bool new_state)
{
  plot_diff = new_state;
  // return to gnuplot the state of the current lattice
  if (new_state) lattice2plot ();
}



#endif
