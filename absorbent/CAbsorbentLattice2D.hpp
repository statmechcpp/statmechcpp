#ifndef CABSORBENTLATTICE2D_H
#define CABSORBENTLATTICE2D_H

/*
 * CSurvivalLattice2D<typename NODE_TYPE,
 *                    bool do_plot>
 *
 * Description:
 *
 * It is based in CLattice2D. It manages a 2D lattice that contains unsigned
 * integer values.
 *
 * This class always knows the quantity of nodes with a particular value.
 * This is useful, for example, for knowing when the system reached the
 * absorbent state without having to check the entire lattice every iteration,
 * saving computation time.
 *
 * It is only expected to take values from 0 to @size-1, being @size the third
 * argument of the constructor (but maybe this could be extended with a few
 * code).
 *
 * Usage:
 *
 * --- Construction ----
 * Instanciate the template using the constructor, for example,
 *
 *   CSurvivalLattice2D<std::vector<int>, true> my_lattice(10, 20, 3);
 *
 * It creates a lattice of 10x20 which will be able to manage 3 different
 * values: {0, 1, 2}
 *
 * The first template parameter indicates the type you will use to pass
 * the coordinates of a node. Hoever, you can alternatively use this class
 * accesing the coordinates individually, without the need of a container.
 *
 * The second template parameter indicates if lattice will be displayed
 * graphically or not. See documentation of CLattice2D for more information,
 * since everything is available.
 *
 * ---- Initialization -----
 * You can initialize the lattice node by node, but take care. If what you want
 * is to place in every node a random valid value (from 0 to n, being n the
 * third argument used in the constructor), just do
 *
 *   my_lattice.uniform_fill ();
 * 
 * and don't worry about anything more. Also, you can safelly call that
 * memeber function at every moment. However, if you want to set the initial
 * values by hand, you have to do the following:
 *
 * 1. Reset any information (even if you have instanciated the object just now)
 * about the abundance of each valid value.
 *
 *   my_lattice.reset_abundances ();
 *
 * 2. Give values to every node of the lattice one by one using one of the
 *    following member functions:
 *
 *   my_lattice.init_value (node, value);
 *   my_lattice.init_value (index1, index2, value);    
 *
 * If you are plotting the lattice, you could be interested in turning off
 * the automatic display while initializing, since it will late a lot.
 *
 *   my_lattice.set_auto_refresh (false);
 *
 *
 * ---- Using the lattice -----
 * Once the lattice has been initialized, you are free to read and change
 * values on it through the functions:
 *
 *  size_t get_value (const NODE_TYPE & node) const;
 *  size_t get_value (INDEX_TYPE i1, INDEX_TYPE i2) const;
 *  void update_value (const NODE_TYPE &, value_type value);
 *  void update_value (index_type index1, index_type index2, value_type value);
 *
 * WARNING: Do NOT use init_value when you mean update_value and viceverse (!).
 *          That would be a complete disaster.
 *
 * ----- Facilities ------
 * You can use all the facilites documented on CLattice2D, and also these one:
 *
 *   my_lattice.monodominace ();
 *
 * That member function will return true or false depending the system is in
 * absorbent state (all the nodes have the same value) or not.
 *
 *   my_lattice.abundance (n);
 *
 * n should be a value inside the range that the lattice can handle.
 */


#include <vector>
#include "../gsl/CGSLRng.hpp"
#include "../lattices/CLattice2D.hpp"

// WARNING:
// kernels impose the NEED of signed values, today
template <typename VALUE_TYPE, bool do_plot = false>
class CAbsorbentLattice2D : public CLattice2D<VALUE_TYPE, do_plot>
{
public:
  typedef typename CLattice2D<VALUE_TYPE, do_plot>::index index;
  typedef typename CLattice2D<VALUE_TYPE, do_plot>::node_type node_type;

private:
  CGSLRng rng;
  const size_t div;
  std::vector<size_t> abundance;

  // set_value member functions of base class are dangerous
  void set_value () {}; // hide by override

public:
  // constructing
  CAbsorbentLattice2D (index dim1, index dim2, size_t div);
  void uniform_fill (); // override to keep abundances

  // write-access to data
  void reset_abundances ();
  void init_value (const node_type &, VALUE_TYPE value);
  void init_value (index index1, index index2, VALUE_TYPE value);
  void update_value (const node_type &, VALUE_TYPE value);
  void update_value (index index1, index index2, VALUE_TYPE value);

  // utilities
  bool monodominance () const;
  size_t get_abundance (size_t i) const {return abundance.at(i);}
};

template <typename VALUE_TYPE, bool do_plot>
CAbsorbentLattice2D<VALUE_TYPE, do_plot>::
CAbsorbentLattice2D (index s1, index s2, size_t idiv)
  : CLattice2D<VALUE_TYPE, do_plot>::CLattice2D (s1, s2, idiv),
    div(idiv),
    abundance(idiv)
{}

// fill lattice with indices between 0 and @n-1
template <typename VALUE_TYPE, bool do_plot>
void
CAbsorbentLattice2D<VALUE_TYPE, do_plot>::
reset_abundances ()
{
  for (size_t i = 0; i != div; i++)
    abundance[i] = 0;
}

// fill lattice with indices between 0 and @n-1
template <typename VALUE_TYPE, bool do_plot>
void
CAbsorbentLattice2D<VALUE_TYPE, do_plot>::
uniform_fill ()
{
  // reset abundances
  reset_abundances ();

  // dirty way of respecting @auto_refresh when filling
  bool prev_auto_refresh;
  if (do_plot)
    {
      prev_auto_refresh = this->is_auto_refresh ();
      this->set_auto_refresh (false);
    }

  // give new random values to the lattice
  for (index i1 = 0; i1 != this->dim1 (); i1++)
    for (index i2 = 0; i2 != this->dim2 (); i2++)
      init_value (i1, i2, rng.uniform_int (div));

  // plot the resulting lattice and set @auto_refresh to the original value
  if (do_plot)
    {
      if (this->is_plot_diff ()) this->lattice2plot ();
      this->set_auto_refresh (prev_auto_refresh);
    }
}



template <typename VALUE_TYPE, bool do_plot>
void
CAbsorbentLattice2D<VALUE_TYPE, do_plot>::
update_value (const node_type & node, VALUE_TYPE new_value)
{
  const VALUE_TYPE current_value = this->get_value (node);
  if (current_value != new_value)
    {
      (abundance.at (current_value))--;
      (abundance.at     (new_value))++;  
      CLattice2D<VALUE_TYPE, do_plot>::set_value (node, new_value);
    }
}

// override set_value for taking count of abundances
template <typename VALUE_TYPE, bool do_plot>
void
CAbsorbentLattice2D<VALUE_TYPE, do_plot>::
update_value (index index1, index index2, VALUE_TYPE new_value)
{
  const VALUE_TYPE current_value = this->get_value (index1, index2);
  if (current_value != new_value)
    {
      (abundance.at (current_value))--;
      (abundance.at     (new_value))++;  
      CLattice2D<VALUE_TYPE, do_plot>::set_value (index1, index2, new_value);
    }
}

// override set_value for taking count of abundances
template <typename VALUE_TYPE, bool do_plot>
void
CAbsorbentLattice2D<VALUE_TYPE, do_plot>::
init_value (const node_type & node, VALUE_TYPE new_value)
{
  (abundance.at (new_value))++;  
  CLattice2D<VALUE_TYPE, do_plot>::
    set_value (node, new_value);
}

// override set_value for taking count of abundances
template <typename VALUE_TYPE, bool do_plot>
void
CAbsorbentLattice2D<VALUE_TYPE, do_plot>::
init_value (index index1, index index2, VALUE_TYPE new_value)
{
  (abundance.at (new_value))++;
  CLattice2D<VALUE_TYPE, do_plot>::set_value (index1, index2, new_value);
}


template <typename VALUE_TYPE, bool do_plot>
bool
CAbsorbentLattice2D<VALUE_TYPE, do_plot>::
monodominance () const
{
  // look a second instance of non-null index
  int alive_indices = 0;
  
  for (size_t i = 0; i != div; ++i)
    {
      if (0 != abundance.at (i))
	if (2 == ++alive_indices)
	    return false;
    }
  return true;
}


#endif
