#ifndef INTERPHASES_HPP
#define INTERPHASES_HPP

namespace observables
{
  /*
   * m_interphases2D:
   *
   * It counts the number of pairs of consecutive nodes which have different
   *  values.
   *
   * When you watch a lattice representation, each node being a square of side
   * 1 and filled with a solid color, it is equivalent to the total perimeter
   * marked by the lines showed by the difference of colors.
   *
   * Notes:
   *
   *  * Not check (by PBC-like simulation) for what is outside the lattice.
   *  * When a node is in the interphase, it is because some of its nearer
   *    nodes are also in the interphase. But we do not want to count twice.
   *    So:    
   *    If a node is counted as being in the interphase, any close node will be
   *    counted only if it had an interphase with a third node which was 
   *    not counted yet (and which will not be counted later...).
   */
  template <typename LATTICE_TYPE>
  size_t m_interphases2D (const LATTICE_TYPE &);

  template <typename LATTICE_TYPE>
  size_t m_interphases2D (const LATTICE_TYPE & a)
  {
    typedef typename LATTICE_TYPE::index index;
    typedef typename LATTICE_TYPE::value_type value_type;

    const size_t first_row = 0, first_column = 0;
    const size_t second_row = 1, second_column = 1;

    //  |----|----|----|----|----|----|--
    // 5| z  < x  < x  < x  < x  < x  | 
    //  |-\/-|-\/-|-\/-|-\/-|-\/-|-\/----
    // 4| z  < x  < x  < x  < x  < x  | 
    //  |-\/-|-\/-|-\/-|-\/-|-\/-|-\/----
    // 3| z  < x  < x  < x  < x  < x  | 
    //  |-\/-|-\/-|-\/-|-\/-|-\/-|-\/----
    // 2| z  < x  < x  < x  < x  < x  | 
    //  |-\/-|-\/-|-\/-|-\/-|-\/-|-\/----
    // 1| z  < x  < x  < x  < x  < x  | 
    //  |-\/-|-\/-|-\/-|-\/-|-\/-|-\/----
    // 0|    < y  < y  < y  < y  < y  | 
    //  |----|----|----|----|----|----|--
    //    0    1    2    3    4    5

    size_t count = 0;
    // We are skipping first file and column (nodes type x above)
    for (index i1 = second_row; i1 < a.dim1 ();
	 i1++)
      for (index i2 = second_column; i2 < a.dim2 ();
	   i2++)  
	{
	  // is current node equal to the left OR down node?
	  const value_type val = a.get_value (i1, i2);
	  if (a.get_value (i1-1, i2) != val) count++;
	  // We do not stop checking if prev is true because reciprocal nodes
	  // will NOT be checked later
	  if (a.get_value (i1, i2-1) != val) count++;
	}

    // Don't avoid first file (nodes type y above)
    for (index i1 = second_row; i1 < a.dim1 (); i1++)
      if (a.get_value (i1, first_column)
	  != a.get_value (i1 - 1, first_column))
	count++;

    // Don't avoid first file (nodes type z above)
    for (index i2 = second_column; i2 < a.dim2 ();
	 i2++)
      if (a.get_value (first_row, i2)
	  != a.get_value (first_row, i2 - 1))
	count++;

    return count;
  }

}

#endif
