#ifndef CSURVIVALLATTICE2D_H
#define CSURVIVALLATTICE2D_H

#include <boost/unordered_map.hpp>
#include "gsl/CGSLRng.hpp"
#include "plot/gnuplot_i.hpp"


template <typename VALUE_TYPE, bool do_plot, typename INDEX_TYPE = long int>
class CSurvivalLattice2D
{
public:

  typedef std::valarray<VALUE_TYPE> node_type;
  

private:

  CGSLRng rng;
  boost::unordered_map<node_type, VALUE_TYPE> umap;
  Gnuplot gp; 
  

public:

  CSurvivalLattice2D (size_t);

};
  
template <typename VALUE_TYPE, bool do_plot, typename INDEX_TYPE>
CSurvivalLattice2D<VALUE_TYPE, do_plot, INDEX_TYPE>
::CSurvivalLattice2D (size_t hint_of_max_cluster_size)
  : rng (),
    umap(hint_of_max_cluster_size)
    
{
  if (do_plot) set_plot ();
}






#endif
